# Beer Tag - Rest API documentation

##Schema

###Location
The API is available at **localhost:8080/api/users** or **localhost:8080/api/beers** with each start of the program.
Responses are sent as JSON.

###Version
Current API version is *'0.0.1-SNAPSHOT'*

###HTTP Verbs

Verb | Description
--- | ---
GET | Retrieving resources
POST | Creating resources
PUT | Updating resources
DELETE | Deleting resources

###Error messages
Do expect proper error message in case of a failure in a given command with detail explanation.

###Supported User commands
**GET**<br /> *all*<br />
Retrieve all users along with all the information they contain, regardless of their status or role.

Example request: localhost:8080/api/users/all

**GET**<br /> *id*<br />
Retrieves all the information regarding a user with the specified ID, as long as it exists in the database.

Example request: localhost:8080/api/users/id/?id=*place id here*

**GET**<br /> *user*<br />
Retrieves all the information regarding a user with the specified username, as long as it exists in the database.

Example request: localhost:8080/api/users/user/?username=*place username here*

**POST**<br /> */*<br />
Creates and adds new user to the database. Username must be unique.

Example request: localhost:8080/api/users/

**PUT**<br /> */*<br />
Updates one of the following fields - First Name, Last Name, Password, Email, Picture as long as the user exist in the database.


Example request: localhost:8080/api/users/

**DELETE**<br /> */*<br />
Inactivate a user based on a given id.

Example request: localhost:8080/api/users/?id=*place the id here*

*Additional fields that are contained by each user:*
- activeYN - Value could be "Y" or "N";
- registrationDate - system date is added with the creation of a user;
- deactivationDate - null by default. Date is updated with the system date when a user is deleted;
- updateDate - Each time a change is done with the information regarding a given user, the date is updated. By default, it is the registration date;

Full JSON example for user creation:<br />
{<br />
    "username": "Krisko",<br />
    "firstName": "Kiril",<br />
    "lastName": "Kirilov",<br />
    "password": "somethingSomething",<br />
    "email": "krisko@beats.com",<br />
    "picture": "https://tse1.mm.bing.net/th?id=OIP.4Atu7LO7r-9yyhWWkm5FnAHaFj&pid=Api&P=0&w=240&h=181"
<br />}<br />

###Supported Beers commands
