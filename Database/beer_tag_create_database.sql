drop database beer_tag;

create database beer_tag;

use beer_tag;

CREATE TABLE `users`
(
    `username` varchar(50) NOT NULL,
    `password` varchar(68) NOT NULL,
    `enabled`  tinyint(4)  NOT NULL,
    PRIMARY KEY (`username`)
);

CREATE TABLE `authorities`
(
    `username`  varchar(50) NOT NULL,
    `userRole` varchar(50) NOT NULL,
    UNIQUE KEY `username_authority` (`username`, `userRole`),
    CONSTRAINT `authorities_fk` FOREIGN KEY (`username`) REFERENCES `users` (`username`)
);

create table users_details
(
    id                int auto_increment
        primary key,
    user_name         varchar(50)              NOT NULL,
    first_name        varchar(20) charset utf8 not null,
    last_name         varchar(20) charset utf8 not null,
    email             varchar(65) charset utf8 not null,
    picture           blob                     null,
    isActive          boolean  DEFAULT true,
    registered_date   datetime DEFAULT CURRENT_TIMESTAMP,
    deactivation_date datetime                 null,
    last_edited_date  datetime DEFAULT CURRENT_TIMESTAMP,
    constraint user_name
        unique (user_name),
    constraint `users_e-mail_uindex`
        unique (email),
    CONSTRAINT `users_details_fk` FOREIGN KEY (`user_name`) REFERENCES `users` (`username`)
);
create table countries
(
    id   int auto_increment
        primary key,
    name varchar(30) charset utf8 not null,
    constraint country_name_uindex
        unique (name)
);
create table styles
(
    id          int auto_increment
        primary key,
    style       varchar(60) charset utf8  not null,
    description varchar(800) charset utf8 null,
    ABV         varchar(20) charset utf8  null,
    IBU         varchar(20) charset utf8  null,
    glassware   varchar(50) charset utf8  null,
    constraint style_title_uindex
        unique (style)
);

create table breweries
(
    id   int auto_increment
        primary key,
    name varchar(120) charset utf8 not null,
    constraint brewery_name_uindex
        unique (name)
);
create table beers
(
    id                int auto_increment
        primary key,
    name              varchar(80) charset utf8  not null,
    description       varchar(600) charset utf8 null,
    country_id        int                       not null,
    brewery_id        int                       not null,
    style_id          int                       not null,
    abv               decimal(4, 2)             not null,
    picture           blob                      null,
    isActive          boolean  DEFAULT true,
    created_by        int                       not null,
    create_date       datetime DEFAULT CURRENT_TIMESTAMP,
    last_edited_date  datetime DEFAULT CURRENT_TIMESTAMP,
    deactivation_date datetime                  null,
    constraint beers_users_id_fk
        foreign key (created_by) references users_details (id),
    constraint brewery_id
        foreign key (brewery_id) references breweries (id),
    constraint country_id
        foreign key (country_id) references countries (id),
    constraint style_id
        foreign key (style_id) references styles (id)

);

create table beer_ratings
(
    id          int auto_increment
        primary key,
    beer_id     int                not null,
    user_id     int                not null,
    rating      int      default 0 null,
    comment     varchar(200)       null,
    create_date datetime DEFAULT CURRENT_TIMESTAMP,
    constraint beer_rating_beers_id_fk
        foreign key (beer_id) references beers (id),
    constraint beer_rating_users_id_fk
        foreign key (user_id) references users_details (id)
);
create table favourite_beers
(
    beer_id int not null,
    user_id int not null,
    primary key (beer_id, user_id),
    constraint beer_id
        foreign key (beer_id) references beers (id),
    constraint user_id
        foreign key (user_id) references users_details (id)
);

create definer = root@localhost view averagebeerrating as
select `b`.`name`                              AS `BeerName`,
       `c`.`name`                              AS `CountryOfOrigin`,
       (sum(`br`.`rating`) / count(`br`.`id`)) AS `AVRGRating`
from ((`beer_tag`.`beers` `b` join `beer_tag`.`countries` `c` on ((`c`.`id` = `b`.`country_id`)))
         join `beer_tag`.`beer_ratings` `br` on ((`b`.`id` = `br`.`beer_id`)))
group by `b`.`name`, `c`.`name`;

create table tags
(
    id  int auto_increment
        primary key,
    tag varchar(30) not null,
    constraint tags_tag_uindex
        unique (tag)
);
create table beer_tags
(
    beer_id int not null,
    tag_id  int not null,
    primary key (beer_id, tag_id),
    constraint beer_tags_beers_id_fk
        foreign key (beer_id) references beers (id),
    constraint beer_tags_tags_id_fk
        foreign key (tag_id) references tags (id)
);


create table drunk_beers
(
    beer_id int not null,
    user_id int not null,
    primary key (beer_id, user_id),
    constraint drunk_beers_beers_id_fk
        foreign key (beer_id) references beers (id),
    constraint drunk_beers_users_id_fk
        foreign key (user_id) references users_details (id)
);

create table user_roles
(
    id   int auto_increment
        primary key,
    role varchar(25) charset utf8 not null,
    constraint user_roles_users_id_fk
        foreign key (id) references users_details (id)
);