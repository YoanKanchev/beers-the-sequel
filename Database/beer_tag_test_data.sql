INSERT INTO `users` (`username`, `password`, `enabled`)
VALUES ('Admin', '{noop}pass1', 1),
       ('Administrator', '{noop}pass2', 1),
       ('Peshov', '{noop}pass3', 1),
       ('mkak', '{noop}pass4', 1);

INSERT INTO beer_tag.users_details (id, user_name, first_name, last_name, email, picture, isActive,
                                    registered_date, deactivation_date, last_edited_date)
VALUES (1, 'Admin', 'Yulian', 'Stefanov', 'something', null, true, '2020-08-30 11:42:31', null,
        now());
INSERT INTO beer_tag.users_details (id, user_name, first_name, last_name, email, picture, isActive,
                                    registered_date, deactivation_date, last_edited_date)
VALUES (2, 'Administrator', 'Yoan', 'Kanchev', 'ykancgev@yahoo.bg', null, true,
        '2020-08-30 11:42:31', null, now());
INSERT INTO beer_tag.users_details (id, user_name, first_name, last_name, email, picture, isActive,
                                    registered_date, deactivation_date, last_edited_date)
VALUES (3, 'Peshov', 'Pesho', 'Peshov', 'becauseI', null, true, '2020-08-30 11:42:31', null, now());
INSERT INTO beer_tag.users_details (id, user_name, first_name, last_name, email, picture, isActive,
                                    registered_date, deactivation_date, last_edited_date)
VALUES (4, 'mkak', 'Bar', 'Barov', 'testing', null, false, '2020-08-30 11:42:31', '2020-09-13 13:50:17',
        '2020-09-13 13:50:17');


INSERT INTO beer_tag.breweries (id, name)
VALUES (1, 'Belhaven Brewery Company Ltd.');
INSERT INTO beer_tag.breweries (id, name)
VALUES (6, 'Brouwerij Duvel Moortgat NV');
INSERT INTO beer_tag.breweries (id, name)
VALUES (7, 'Brouwerij Van Steenberge N.V.');
INSERT INTO beer_tag.breweries (id, name)
VALUES (9, 'Budějovický Budvar, n.p.');
INSERT INTO beer_tag.breweries (id, name)
VALUES (2, 'Long Trail Brewing Co.');
INSERT INTO beer_tag.breweries (id, name)
VALUES (8, 'Mikkeller ApS');
INSERT INTO beer_tag.breweries (id, name)
VALUES (4, 'Picobrouwerij Alvinne');
INSERT INTO beer_tag.breweries (id, name)
VALUES (10, 'Pivovary Staropramen');
INSERT INTO beer_tag.breweries (id, name)
VALUES (3, 'Rogue Ales');

INSERT INTO beer_tag.breweries (id, name)
VALUES (5, 'The Alchemist');
INSERT INTO beer_tag.countries (id, name)
VALUES (3, 'Belgium');
INSERT INTO beer_tag.countries (id, name)
VALUES (5, 'Czech Republic');
INSERT INTO beer_tag.countries (id, name)
VALUES (4, 'Denmark');
INSERT INTO beer_tag.countries (id, name)
VALUES (1, 'Scotland');
INSERT INTO beer_tag.countries (id, name)
VALUES (2, 'USA');


INSERT INTO beer_tag.styles (id, style, description, ABV, IBU, glassware)
VALUES (1, 'Scottish Ale',
        'Scottish-Style Ales vary depending on strength and flavor, but in general retain a malt-forward character with some degree of caramel-like malt flavors and a soft and chewy mouthfeel. Some examples feature a light smoked peat flavor. Hops do not play a huge role in this style. The numbers commonly associated with brands of this style (60/70/80 and others) reflect the Scottish tradition of listing the cost, in shillings, of a hogshead (large cask) of beer. Overly smoked versions would be considered specialty examples. Smoke or peat should be restrained.',
        '2.8-5.3', '9-25', 'Tulip');
INSERT INTO beer_tag.styles (id, style, description, ABV, IBU, glassware)
VALUES (2, 'Winter Warmer',
        'These malty, sweet offerings tend to be a favorite winter seasonal. A big malt presence, both in flavor and body, leads the way. The color of this style ranges from brownish reds to nearly pitch black. Hop bitterness is generally low and balanced, but hop character can be pronounced in the aroma. Alcohol warmth is not uncommon.',
        '5.5–8.0', '35–50', 'Goblet or Chalice');
INSERT INTO beer_tag.styles (id, style, description, ABV, IBU, glassware)
VALUES (3, 'IPA - Belgian',
        'Inspired by American India Pale Ales and Double IPAs, more and more Belgian brewers (like Chouffe & Urthel) are brewing hoppy pale colored ales for the US market. There''s been an increase of American brewers making Belgian IPAs, too. Generally, Belgian IPAs are considered too hoppy by Belgian beer drinkers. Various malts are used, but the beers of this style are finished with Belgian yeast strains (bottle-conditioned) while the hops employed tend to be American.',
        '6.0-11.0', '50–80', 'Tulip');
INSERT INTO beer_tag.styles (id, style, description, ABV, IBU, glassware)
VALUES (4, 'IPA - New England',
        'Emphasizing hop aroma and flavor without bracing bitterness, the New England IPA leans heavily on late and dry hopping techniques to deliver a bursting juicy, tropical hop experience.',
        '6.3–7.5', '50–70', 'Pint Glass');
INSERT INTO beer_tag.styles (id, style, description, ABV, IBU, glassware)
VALUES (5, 'Blonde Ale - Belgian',
        'The Belgian-style blonde ale is typically easy-drinking, with a low but pleasing hop bitterness. This is a light- to medium-bodied ale, with a low malt aroma that has a spiced and sometimes fruity-ester character. Sugar is sometimes added to lighten the perceived body. This style is medium in sweetness and not as bitter as Belgian-style tripels or golden strong ales. It is usually brilliantly clear.',
        '6.3–7.9', '15–30', 'Tulip');
INSERT INTO beer_tag.styles (id, style, description, ABV, IBU, glassware)
VALUES (6, 'Strong Ale - Belgian Pale',
        'Like a Belgian Pale Ale, the strong versions will also be pale straw to deep golden in color. What sets them apart is a much higher alcohol content that can range from deliciously hidden to devastatingly present. Expect a complex and powerful ale, yet delicate with rounded flavors and a big, billowy, rocky, white head.',
        '7.0–12.0', '20–40', 'Goblet');
INSERT INTO beer_tag.styles (id, style, description, ABV, IBU, glassware)
VALUES (7, 'IPA - Imperial',
        'We have west coast American brewers to thank for this somewhat reactionary style. Take an India Pale Ale and feed it steroids, and you''ll end up with a Double or Imperial IPA. Although generally recognizable alongside its sister styles in the IPA family, you should expect something more robust, malty, and alcoholic with a characteristically intense hop profile in both taste and aroma.',
        '7.0–12.0', '65–100', 'Tulip');
INSERT INTO beer_tag.styles (id, style, description, ABV, IBU, glassware)
VALUES (8, 'Pilsner - Bohemian / Czech',
        'The Bohemian Pilsener has a slightly sweet and evident malt character and a toasted, biscuit-like, bready malt character. Hop bitterness is perceived as medium with a low to medium-low level of noble-type hop aroma and flavor.',
        '4.1-5.1', '30-45', 'Pilsener');


INSERT INTO beer_tag.beers (id, name, description, country_id, brewery_id, style_id, abv, picture, active, created_by, create_date, last_edited_date, deactivation_date) VALUES (1, 'St. Andrew''s Ale', 'On the bottle it tells you that the beer was “inspired by the great Scottish invention: golf”, and there is picture of a golfer with a green blazer swinging at a ball.', 1, 1, 1, 5.00, 'https://assets.greenekingshop.co.uk/wp-content/gkshop/uploads/20191007131306/Belhaven-St-Andrews-1.jpg', 1, 1, '2020-08-30 11:42:31', '2020-09-27 20:36:51', null);
INSERT INTO beer_tag.beers (id, name, description, country_id, brewery_id, style_id, abv, picture, active, created_by, create_date, last_edited_date, deactivation_date) VALUES (2, 'Hibernator', 'Built to take the bite from a cold winter night, Hibernator features a rich malt base highlighted by warming notes of caramel and toffee. Don''t sleep on this rich, robust winter brew.', 2, 2, 1, 6.00, 'https://cdn.beeradvocate.com/im/beers/271.jpg', 1, 2, '2020-08-30 11:42:31', '2020-08-30 11:42:31', null);
INSERT INTO beer_tag.beers (id, name, description, country_id, brewery_id, style_id, abv, picture, active, created_by, create_date, last_edited_date, deactivation_date) VALUES (3, 'Mogul Madness Ale', null, 2, 3, 2, 6.60, 'https://www.winefolder.com/media/catalog/product/cache/1/image/650x650/9df78eab33525d08d6e5fb8d27136e95/r/o/rogue_mogul_madness_ale.jpg', 1, 3, '2020-08-30 11:42:31', '2020-08-30 11:42:31', null);
INSERT INTO beer_tag.beers (id, name, description, country_id, brewery_id, style_id, abv, picture, active, created_by, create_date, last_edited_date, deactivation_date) VALUES (4, 'Gaspar Ale', null, 3, 4, 3, 8.00, 'https://img.saveur-biere.com/img/p/7032-13433.jpg', 1, 4, '2020-08-30 11:42:31', '2020-08-30 11:42:31', null);
INSERT INTO beer_tag.beers (id, name, description, country_id, brewery_id, style_id, abv, picture, active, created_by, create_date, last_edited_date, deactivation_date) VALUES (5, 'Crusher', 'The Crusher is an American Double IPA that I started making years ago at the old pub. Hop heads were constantly asking for more hops. So, in the words of Frank Zappa, "Did you say you want some more? Well here''s some more." While I enjoy hops with the rest of them, I still try to maintain some semblance of balance and drinkability. This beer is oozing with hop flavor and aroma with a very dry finish. Enjoy responsibly, this one can sneak up on you.', 2, 5, 4, 8.00, 'https://cdn.beeradvocate.com/im/beers/114804.jpg', 1, 3, '2020-08-30 11:42:31', '2020-08-30 11:42:31', null);
INSERT INTO beer_tag.beers (id, name, description, country_id, brewery_id, style_id, abv, picture, active, created_by, create_date, last_edited_date, deactivation_date) VALUES (6, 'Focal Banger', 'An American IPA, hopped judiciously with Citra and Mosaic.', 2, 5, 4, 7.00, 'https://products2.imgix.drizly.com/ci-the-alchemist-focal-banger-ipa-6a6216de31a62e21.png?auto=format%2Ccompress&fm=jpg&q=20', 1, 2, '2020-08-30 11:42:31', '2020-08-30 11:42:31', null);
INSERT INTO beer_tag.beers (id, name, description, country_id, brewery_id, style_id, abv, picture, active, created_by, create_date, last_edited_date, deactivation_date) VALUES (7, 'Heady Topper', 'Heddy Topper is an American Double India Pale Ale. This beer is not intended to be the biggest or most bitter. It is meant to give you wave after wave of hoppy goodness on your palate. Tremendous amounts of American hops will creep up on you, and leave you with a dense hoppy finish in you mouth. So drinkable, it''s scary. Sometimes I wish I could crawl right into the can.', 2, 5, 4, 8.00, 'https://upload.wikimedia.org/wikipedia/commons/thumb/1/1f/Headytopper.jpg/220px-Headytopper.jpg', 1, 2, '2020-08-30 11:42:31', '2020-08-30 11:42:31', null);
INSERT INTO beer_tag.beers (id, name, description, country_id, brewery_id, style_id, abv, picture, active, created_by, create_date, last_edited_date, deactivation_date) VALUES (8, 'Maredsous 6 - Blonde', null, 3, 6, 5, 6.00, 'https://www.gourmetencasa-tcm.com/5459/maredsous-6-blonde-33cl-caja-24-und.jpg', 1, 4, '2020-08-30 11:42:31', '2020-08-30 11:42:31', null);
INSERT INTO beer_tag.beers (id, name, description, country_id, brewery_id, style_id, abv, picture, active, created_by, create_date, last_edited_date, deactivation_date) VALUES (9, 'Duvel Barrel Aged', 'Our brewers have been conducting an exciting taste experiment: maturing Duvel in old, wooden barrels.', 3, 6, 6, 11.50, 'https://www.bodecall.com/images/stories/virtuemart/product/duvel-barrel-aged.png', 1, 1, '2020-08-30 11:42:31', '2020-08-30 11:42:31', null);
INSERT INTO beer_tag.beers (id, name, description, country_id, brewery_id, style_id, abv, picture, active, created_by, create_date, last_edited_date, deactivation_date) VALUES (10, 'Diablesa 666 Blonde', null, 3, 7, 6, 7.30, 'https://images-na.ssl-images-amazon.com/images/I/51VGvJ06ScL._AC_SX342_.jpg', 1, 3, '2020-08-30 11:42:31', '2020-08-30 11:42:31', null);
INSERT INTO beer_tag.beers (id, name, description, country_id, brewery_id, style_id, abv, picture, active, created_by, create_date, last_edited_date, deactivation_date) VALUES (11, 'Waagstuk', null, 3, 7, 6, 11.00, 'https://img.stramis.cz/images/Staropramen%20Jeden%C3%A1ctka%200,5l.jpg', 1, 2, '2020-08-30 11:42:31', '2020-08-30 11:42:31', null);
INSERT INTO beer_tag.beers (id, name, description, country_id, brewery_id, style_id, abv, picture, active, created_by, create_date, last_edited_date, deactivation_date) VALUES (12, 'Atomium Grand Cru', 'A unique beer as grand cru goes, flavorful and different.', 3, 7, 6, 8.00, 'https://dydza6t6xitx6.cloudfront.net/ci-atomium-premier-grand-cru-c69cacba134b7740.jpeg', 1, 4, '2020-08-30 11:42:31', '2020-08-30 11:42:31', null);
INSERT INTO beer_tag.beers (id, name, description, country_id, brewery_id, style_id, abv, picture, active, created_by, create_date, last_edited_date, deactivation_date) VALUES (13, 'Cascade Imperial India Pale Ale', null, 4, 8, 7, 9.00, 'https://cdn.beeradvocate.com/im/beers/269562.jpg', 1, 2, '2020-08-30 11:42:31', '2020-09-26 20:58:26', '2020-09-26 20:58:26');
INSERT INTO beer_tag.beers (id, name, description, country_id, brewery_id, style_id, abv, picture, active, created_by, create_date, last_edited_date, deactivation_date) VALUES (14, '1000 IBU', null, 4, 8, 7, 9.60, 'https://res.cloudinary.com/ratebeer/image/upload/w_250,c_limit/beer_118986.jpg', 1, 1, '2020-08-30 11:42:31', '2020-08-30 11:42:31', null);
INSERT INTO beer_tag.beers (id, name, description, country_id, brewery_id, style_id, abv, picture, active, created_by, create_date, last_edited_date, deactivation_date) VALUES (15, '0 IBU', null, 4, 8, 7, 12.10, 'https://cdn.shopify.com/s/files/1/1268/5569/products/0ibu_600x600.jpg?v=1606474213', 1, 1, '2020-08-30 11:42:31', '2020-08-30 11:42:31', null);
INSERT INTO beer_tag.beers (id, name, description, country_id, brewery_id, style_id, abv, picture, active, created_by, create_date, last_edited_date, deactivation_date) VALUES (16, '3000 IBU', null, 4, 8, 7, 15.50, 'https://untappd.akamaized.net/site/beer_logos_hd/beer-3407830_c84e6_hd.jpeg', null, 3, '2020-08-30 11:42:31', '2020-08-30 11:42:31', null);
INSERT INTO beer_tag.beers (id, name, description, country_id, brewery_id, style_id, abv, picture, active, created_by, create_date, last_edited_date, deactivation_date) VALUES (17, 'Budvar 33 Ležák', null, 5, 9, 8, 4.60, 'https://assets-global.website-files.com/5c62b6e570613fd36f9e6ba2/5f6454f47f4e2d760d12f61e_Budvar_bottle_33_0%2C50_HiRes_RGB_s%402x.png', 1, 2, '2020-08-30 11:42:31', '2020-08-30 11:42:31', null);
INSERT INTO beer_tag.beers (id, name, description, country_id, brewery_id, style_id, abv, picture, active, created_by, create_date, last_edited_date, deactivation_date) VALUES (18, 'Budweiser Budvar B:CLASSIC Světlé Výčepní Pivo', 'udějovický Budvar je dnes moderní a akční firmou, která je postavena na pevných základech. Dokázal obstát v tvrdém boji s konkurencí.', 5, 9, 8, 4.00, 'https://cz5.staticac.cz/foto/produkty/015250/15195.jpg', 1, 2, '2020-08-30 11:42:31', '2020-08-30 11:42:31', null);
INSERT INTO beer_tag.beers (id, name, description, country_id, brewery_id, style_id, abv, picture, active, created_by, create_date, last_edited_date, deactivation_date) VALUES (19, 'Staropramen Lager', 'Staropramen has a thick, off-white head and a clear, somewhat bubbly, golden appearance, with thin lacing left behind. The aroma is of sweet, biscuit and bready malt, with a touch of hop zing.', 5, 10, 8, 5.00, 'https://napitkite.bg/wp-content/uploads/2018/04/staropramen-staklo-0.5.png', 1, 3, '2020-08-30 11:42:31', '2020-08-30 11:42:31', null);
INSERT INTO beer_tag.beers (id, name, description, country_id, brewery_id, style_id, abv, picture, active, created_by, create_date, last_edited_date, deactivation_date) VALUES (20, 'Staropramen 11° (Jedenáctka)', null, 5, 10, 8, 4.70, 'https://img.stramis.cz/images/Staropramen%20Jeden%C3%A1ctka%200,5l.jpg', 1, 1, '2020-08-30 11:42:31', '2020-08-30 11:42:31', null);
INSERT INTO beer_tag.beers (id, name, description, country_id, brewery_id, style_id, abv, picture, active, created_by, create_date, last_edited_date, deactivation_date) VALUES (21, 'Alebale', '', 4, 11, 2, 9.00, 'https://www.bsmc.net.au/wp-content/uploads/No-image-available.jpg', 0, 1, '2021-02-13 16:31:50', '2021-02-13 16:31:50', null);
INSERT INTO beer_tag.beers (id, name, description, country_id, brewery_id, style_id, abv, picture, active, created_by, create_date, last_edited_date, deactivation_date) VALUES (22, 'Blelalwlsf', '<null>', 3, 12, 4, 12.00, 'https://www.bsmc.net.au/wp-content/uploads/No-image-available.jpg', 1, 1, '2021-02-13 16:35:51', '2021-02-13 16:35:51', null);

INSERT INTO beer_tag.beer_ratings (id, beer_id, user_id, rating, comment)
VALUES (1, 3, 3, 5, null);
INSERT INTO beer_tag.beer_ratings (id, beer_id, user_id, rating, comment)
VALUES (2, 3, 4, 7, null);
INSERT INTO beer_tag.beer_ratings (id, beer_id, user_id, rating, comment)
VALUES (3, 3, 2, 6, null);
INSERT INTO beer_tag.beer_ratings (id, beer_id, user_id, rating, comment)
VALUES (4, 2, 2, 2, null);
INSERT INTO beer_tag.beer_ratings (id, beer_id, user_id, rating, comment)
VALUES (5, 1, 3, 7, null);
INSERT INTO beer_tag.beer_ratings (id, beer_id, user_id, rating, comment)
VALUES (6, 7, 1, 9, null);
INSERT INTO beer_tag.beer_ratings (id, beer_id, user_id, rating, comment)
VALUES (7, 9, 3, 10, 'This is the one best thing I have ever tasted!!1');
INSERT INTO beer_tag.beer_ratings (id, beer_id, user_id, rating, comment)
VALUES (8, 12, 1, 4, null);
INSERT INTO beer_tag.beer_ratings (id, beer_id, user_id, rating, comment)
VALUES (9, 5, 4, 5, null);
INSERT INTO beer_tag.beer_ratings (id, beer_id, user_id, rating, comment)
VALUES (10, 11, 3, 4, null);
INSERT INTO beer_tag.beer_ratings (id, beer_id, user_id, rating, comment)
VALUES (11, 3, 1, 7, null);
INSERT INTO beer_tag.beer_ratings (id, beer_id, user_id, rating, comment)
VALUES (12, 10, 2, 5, null);
INSERT INTO beer_tag.beer_ratings (id, beer_id, user_id, rating, comment)
VALUES (13, 13, 3, 9, 'One word...Amazing!');
INSERT INTO beer_tag.beer_ratings (id, beer_id, user_id, rating, comment)
VALUES (14, 16, 4, 5, null);
INSERT INTO beer_tag.beer_ratings (id, beer_id, user_id, rating, comment)
VALUES (15, 10, 1, 6, null);
INSERT INTO beer_tag.beer_ratings (id, beer_id, user_id, rating, comment)
VALUES (16, 15, 4, 4, null);
INSERT INTO beer_tag.beer_ratings (id, beer_id, user_id, rating, comment)
VALUES (17, 16, 3, 7, null);
INSERT INTO beer_tag.beer_ratings (id, beer_id, user_id, rating, comment)
VALUES (18, 12, 2, 1, null);
INSERT INTO beer_tag.beer_ratings (id, beer_id, user_id, rating, comment)
VALUES (19, 9, 1, 2, null);
INSERT INTO beer_tag.beer_ratings (id, beer_id, user_id, rating, comment)
VALUES (20, 15, 3, 3, null);
INSERT INTO beer_tag.beer_ratings (id, beer_id, user_id, rating, comment)
VALUES (21, 7, 2, 5, null);
INSERT INTO beer_tag.beer_ratings (id, beer_id, user_id, rating, comment)
VALUES (22, 17, 3, 1, 'Disgusting!!!');
INSERT INTO beer_tag.beer_ratings (id, beer_id, user_id, rating, comment)
VALUES (23, 16, 2, 7, 'Great!');
INSERT INTO beer_tag.beer_ratings (id, beer_id, user_id, rating, comment)
VALUES (24, 3, 1, 5, 'nothing special');
INSERT INTO beer_tag.beer_ratings (id, beer_id, user_id, rating, comment)
VALUES (25, 11, 2, 5, 'Ok');
INSERT INTO beer_tag.beer_ratings (id, beer_id, user_id, rating, comment)
VALUES (26, 16, 1, 4, 'it brings me to my childhood....in a bad way!');
INSERT INTO beer_tag.beer_ratings (id, beer_id, user_id, rating, comment)
VALUES (27, 13, 4, 6, 'This beer is kinda meh!');
INSERT INTO beer_tag.beer_ratings (id, beer_id, user_id, rating, comment)
VALUES (28, 12, 1, 7, 'This beer is almost perfect!');


INSERT INTO beer_tag.drunk_beers (beer_id, user_id)
VALUES (2, 3);
INSERT INTO beer_tag.drunk_beers (beer_id, user_id)
VALUES (3, 3);
INSERT INTO beer_tag.drunk_beers (beer_id, user_id)
VALUES (3, 4);
INSERT INTO beer_tag.drunk_beers (beer_id, user_id)
VALUES (5, 3);
INSERT INTO beer_tag.drunk_beers (beer_id, user_id)
VALUES (5, 4);
INSERT INTO beer_tag.drunk_beers (beer_id, user_id)
VALUES (7, 2);
INSERT INTO beer_tag.drunk_beers (beer_id, user_id)
VALUES (8, 4);
INSERT INTO beer_tag.drunk_beers (beer_id, user_id)
VALUES (10, 4);
INSERT INTO beer_tag.drunk_beers (beer_id, user_id)
VALUES (11, 1);
INSERT INTO beer_tag.drunk_beers (beer_id, user_id)
VALUES (14, 4);
INSERT INTO beer_tag.drunk_beers (beer_id, user_id)
VALUES (15, 3);


INSERT INTO beer_tag.favourite_beers (beer_id, user_id)
VALUES (2, 3);
INSERT INTO beer_tag.favourite_beers (beer_id, user_id)
VALUES (3, 3);
INSERT INTO beer_tag.favourite_beers (beer_id, user_id)
VALUES (3, 4);
INSERT INTO beer_tag.favourite_beers (beer_id, user_id)
VALUES (5, 3);
INSERT INTO beer_tag.favourite_beers (beer_id, user_id)
VALUES (5, 4);
INSERT INTO beer_tag.favourite_beers (beer_id, user_id)
VALUES (7, 2);
INSERT INTO beer_tag.favourite_beers (beer_id, user_id)
VALUES (8, 4);
INSERT INTO beer_tag.favourite_beers (beer_id, user_id)
VALUES (10, 4);
INSERT INTO beer_tag.favourite_beers (beer_id, user_id)
VALUES (11, 1);
INSERT INTO beer_tag.favourite_beers (beer_id, user_id)
VALUES (14, 4);
INSERT INTO beer_tag.favourite_beers (beer_id, user_id)
VALUES (15, 3);

INSERT INTO beer_tag.user_roles (id, role)
VALUES (1, 'administrator');
INSERT INTO beer_tag.user_roles (id, role)
VALUES (2, 'regular');
INSERT INTO beer_tag.user_roles (id, role)
VALUES (3, 'guest');

INSERT INTO beer_tag.tags (tag)
VALUES ('clear'),
       ('creamy'),
       ('fruit'),
       ('coffee'),
       ('chocolate'),
       ('herbs'),
       ('spicy'),
       ('homemade'),
       ('sour'),
       ('chilly'),
       ('hazy');

INSERT INTO beer_tag.beer_tags(beer_id, tag_id)

VALUES (1, 5),
       (3, 2),
       (4, 1),
       (3, 5),
       (2, 5),
       (2, 1),
       (1, 1);


INSERT INTO `authorities` (`username`, `userRole`) VALUES
('Admin', 'ROLE_USER'),
('Administrator', 'ROLE_USER'),
('Admin', 'ROLE_ADMIN'),
('Administrator', 'ROLE_ADMIN'),
('mkak', 'ROLE_USER'),
('Peshov', 'ROLE_USER');