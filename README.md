# Welcome to Beer_Tag
### Overview

Beer tag is an interactive web based app, that gives its users the ability to create, read, update and delete their favorite beers. Each beer has detailed information about it from the ABV (alcohol by volume) to the style and description. 
Each member of the app can add new beers and there is a rating system for the more competitive ones.

### 1.The Database
The data of the application is stored on the **MariaDB** database server and can be easly recreated by the two files in the Database folder : 
- **beer_tag_create_database.sql** - responsible for creating the tables of the database;
- **beer_tag_test_data.sql** - inserting some test data in the database.

The database is connected to the java code using **Hibernate**.


------------
##### Issues/bugs

No known bugs for the time being

------------

#### Backend
First and foremost in the building of the app we tried hard to follow all of the OOP principles.
After that, with the help of the **SpringMVC** and **SpringBoot** frameworks and using **Spring Security** to handle user registration and user roles, we build the structure in tree layers:
- ##### Repository layer
This is the layer in whitch the connection between the java code and the database hapens with **Hibernate**
- ##### Service layer
In this layer there is the buisness logic of the app. Here you will find mostly validations for the beers or the users, that are logged in and what rights do they have to modify the beers.
- ##### Controller layer
This is the layer that makes the connection with the client, and it branches out to **Rest controller** and **MVC**

#### Frontend
The frontend on **Beer_Tag** is written on **HTML** and **CSS** with the Html pages connecting with the **MVC** controller via the **Thymeleaf** template.
The front end of the website is design to soft delete an entry and any search performed action against the database afterwards for such entry will return an error "**** not found".

#### Functionallity
### 2.The Beers
I.Each beer has :

- Name, 
- Description,
- Tags (with its own structure and operations),
- Country (with it own structure and operations),
- Brewery(with it own structure and CRUD operations),
- Style(with it own structure and CRUD operations),
- Abv,
- CreatedBy(User),
- Picture

II.On each beer there can be aplied CRUD operations, they can be listed by

-  country, style, tags
and can be sorted by
- name, ABV, rating

III.Each beer can be rated and commented on

------------
##### Issues/Bugs
- When searching by two tags the result is inconsistent.

------------
### 3.The Users

User registration and authentication is handled by **Spring Security**.<br/>
Certain pages and functionalities in the web module require a user to be authenticated in order to have access. 
Pages like Create or Edit Beer, Profile page and Profile settings menu. Each regular user has limited rights to edit its own created beers and profile.<br/>
An additional branch of the web module provides access to edit and delete every beer and user. Access on admin level is requiered and such can be given or taken
only by another user with admin authentication through the admin portal.<br/>
<br/>
As part of Spring Security, the user data storage is splitted into 3 tables at the backend.<br/>
Table **user_details** stores detailed information for each user like User ID, Username, First and Last Name, Email, Profile Picture, Date of Creation, Last Update and Deactivation Date, User Status.<br/>
Table **users** stores information regarding the password for each user. It can be connected with the main user table via username acting as primary key.<br/>
Table **authorities** acts like storage for level of access each user have. It again can be connected to the main user table via username acting as primary key.<br/>
<br/>
I. The **Rest API** support the basic CRUD operations through the following:
We can **GET** all users, search user by ID or Username, find the sign in user Drunk or Favourite beer list.<br/>
We can **POST** a new user, add a beer to the Drunk or Favourite list of a logged user.<br/>
We can **PUT** a new First or Last name, password to the logged user.<br/>
We can **DELETE** a user or remove a beer from the Drunk or Favourite list.<br/>
<br/>
II. The **MVC** has a separated controller handling different part of the user interface.<br/>
The **User Controller** supports 2 user pages. One handling profile changes the logged user can do on his profile, including self deactivation. The other
supporting and managing the list of beers a user has.<br/>
The **Registration and LogIn Controllers** hold the process of creating and authenticating the users.<br/>
The **Admin Portal Controller** support the editing pages of each user or beer along with a search bar along with giving or taking rights.<br/>

------------
##### Issues/Bugs
- When searching for a beer to edit, after submitting the change, the old and new name appear in the text field. Operation
is successful..

------------

















