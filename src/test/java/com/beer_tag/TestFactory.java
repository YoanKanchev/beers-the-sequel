package com.beer_tag;

import com.beer_tag.models.dto.beers.CreateBeerDto;
import com.beer_tag.models.enums.Role;
import com.beer_tag.models.impl.Beer;
import com.beer_tag.models.impl.Brewery;
import com.beer_tag.models.impl.Country;
import com.beer_tag.models.impl.Rating;
import com.beer_tag.models.impl.Style;
import com.beer_tag.models.impl.Tag;
import com.beer_tag.models.impl.User;
import com.beer_tag.models.impl.UserRole;

public class TestFactory {

		public static final String TEST_STRING = "TEST";
		public static final int    TEST_ID     = 1;

		public static CreateBeerDto createTestCreateBeerDto() {
				CreateBeerDto beerDto = new CreateBeerDto();
				beerDto.setId(TEST_ID);
				beerDto.setName(TEST_STRING);
				beerDto.setBreweryId(TEST_ID);
				beerDto.setStyleId(TEST_ID);
				return beerDto;
		}

		public static Country createTestCountry() {
				return new Country(TEST_ID, TEST_STRING);
		}

		public static Brewery createTestBrewery() {
				Brewery brewery = new Brewery();
				brewery.setId(TEST_ID);
				brewery.setName(TEST_STRING);
				brewery.setShortDescription(TEST_STRING);
				brewery.setPicture(TEST_STRING);
				brewery.setFoundedDate(TEST_STRING);
				brewery.setCountry(createTestCountry());

				return brewery;
		}

		public static Style createTestStyle() {
				return new Style(TEST_ID, TEST_STRING, TEST_STRING, TEST_STRING, TEST_STRING,
						TEST_STRING);
		}

		public static Tag createTestTag() {
				return new Tag(TEST_ID, TEST_STRING);
		}

		public static Rating createTestRating() {
				return new Rating();
		}

		public static User createTestUser() {
				User user = new User();
				user.setId(TEST_ID);
				user.setUsername(TEST_STRING);
				user.setEnabled(true);
				user.setPassword(TEST_STRING);
				return new User();
		}

		public static UserRole createTestUserAuthority() {
				return new UserRole(createTestUser(),TEST_STRING);
		}

		public static Beer createTestBeer(User creator) {
				Beer beer = new Beer();
				beer.setName(TEST_STRING);
				beer.setAbv(TEST_ID);
				beer.setBrewery(createTestBrewery());
				beer.setCreatedBy(creator);
				beer.setDescription(TEST_STRING);
				beer.setPicture(TEST_STRING);
				beer.setStyle(createTestStyle());
				return beer;
		}

}
