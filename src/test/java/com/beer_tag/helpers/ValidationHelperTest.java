package com.beer_tag.helpers;

import com.beer_tag.repositories.BeerRepository;
import com.beer_tag.repositories.UserRepository;
import com.beer_tag.services.validation.UserValidationService;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.crypto.password.PasswordEncoder;

@ExtendWith(MockitoExtension.class)
public class ValidationHelperTest {

		@InjectMocks
		UserValidationService userValidationService;
		@Mock
		BeerRepository        beerRepository;
		@Mock
		UserRepository        userRepository;
		@Mock
		PasswordEncoder       passwordEncoder;

}
