package com.beer_tag.services;


import com.beer_tag.exceptions.AuthorisationException;
import com.beer_tag.exceptions.EntityNotFoundException;
import com.beer_tag.models.dto.beers.CreateBeerDto;
import com.beer_tag.models.impl.*;
import com.beer_tag.repositories.BeerRepository;
import com.beer_tag.repositories.TagRepository;
import com.beer_tag.services.mapping.ModelMapperService;
import com.beer_tag.services.validation.BaseValidationsService;
import com.beer_tag.services.validation.BeerValidationService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static com.beer_tag.TestFactory.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class BeerServiceActionsTests {

    @InjectMocks
    BeerServiceActionsImpl beerServiceActions;
    @Mock
    BeerServiceFinderImpl beerServiceFinder;
    @Mock
    BeerRepository beerRepository;
    @Mock
    BreweryService breweryService;
    @Mock
    StyleService styleService;
    @Mock
    CountryService countryService;
    @Mock
    TagService tagService;
    @Mock
    ModelMapperService modelMapper;
    @Mock
    BeerValidationService beerValidationService;
    @Mock
    BaseValidationsService baseValidationsService;

    @Test
    public void create_Should_Succeed() {
        //Arrange
        CreateBeerDto createBeerDto = createTestCreateBeerDto();
        User user = createTestUser();
        Beer beer = createTestBeer(user);
        Brewery brewery = createTestBrewery();
        Style style = createTestStyle();

        doNothing().when(beerValidationService).validateNameDuplication(createBeerDto);
        when(styleService.getById(createBeerDto.getStyleId())).thenReturn(style);
        when(breweryService.getById(createBeerDto.getBreweryId())).thenReturn(brewery);
        when(modelMapper.toBeer(createBeerDto, user, style, brewery)).thenReturn(beer);
        when(beerRepository.save(beer)).thenReturn(beer);
        //Act
        Beer beerTest = beerServiceActions.create(createBeerDto, user);
        // Assert
        assertEquals(beer, beerTest);
    }

    @Test
    public void create_Should_Fail_When_Not_Valid() {
        //Arrange
        CreateBeerDto createBeerDto = createTestCreateBeerDto();
        User user = createTestUser();

        doThrow(AuthorisationException.class).when(beerValidationService).validateNameDuplication(createBeerDto);
        //Act
        // Assert
        assertThrows(AuthorisationException.class, () -> beerServiceActions.create(createBeerDto, user));
    }

    @Test
    public void deactivate_Should_Succeed() {
        //Arrange
        int id = anyInt();
        User user = createTestUser();
        Beer beer = createTestBeer(user);
        beer.setActive(true);

        when(beerServiceFinder.getById(id)).thenReturn(beer);
        doNothing().when(baseValidationsService).adminORCreatorCheck(beer, user);
        when(beerRepository.save(beer)).thenReturn(beer);
        //Act
        beerServiceActions.deactivate(id, user);
        // Assert
        assertFalse(beer.getActive());
    }

    @Test
    public void deactivate_Should_Fail_When_Not_Valid_User() {
        //Arrange
        int id = anyInt();
        User user = createTestUser();
        Beer beer = createTestBeer(user);

        when(beerServiceFinder.getById(id)).thenReturn(beer);
        doThrow(AuthorisationException.class).when(baseValidationsService).adminORCreatorCheck(beer, user);
        //Act
        // Assert
        assertThrows(AuthorisationException.class, () -> beerServiceActions.deactivate(id, user));
    }

    @Test
    public void deactivate_Should_Fail_When_Not_Valid_Beer() {
        //Arrange
        int id = anyInt();
        User user = createTestUser();

        when(beerServiceFinder.getById(id)).thenThrow(EntityNotFoundException.class);
        //Act
        // Assert
        assertThrows(EntityNotFoundException.class, () -> beerServiceActions.deactivate(id, user));
    }

    @Test
    public void activate_Should_Succeed() {
        //Arrange
        int id = anyInt();
        User user = createTestUser();
        Beer beer = createTestBeer(user);
        beer.setActive(false);

        when(beerServiceFinder.getById(id)).thenReturn(beer);
        doNothing().when(baseValidationsService).adminORCreatorCheck(beer, user);
        when(beerRepository.save(beer)).thenReturn(beer);
        //Act
        beerServiceActions.activate(id, user);
        // Assert
        assertTrue(beer.getActive());
    }

    @Test
    public void update_Should_Succeed() {
        //Arrange
        int id = anyInt();
        CreateBeerDto createBeerDto = createTestCreateBeerDto();
        User user = createTestUser();
        Beer beer = createTestBeer(user);
        Brewery brewery = createTestBrewery();
        Style style = createTestStyle();

        when(beerServiceFinder.getById(id)).thenReturn(beer);
        when(breweryService.getById(createBeerDto.getBreweryId())).thenReturn(brewery);
        when(styleService.getById(createBeerDto.getStyleId())).thenReturn(style);
        when(modelMapper.toBeer(createBeerDto, beer, style, brewery)).thenReturn(beer);
        doNothing().when(baseValidationsService).adminORCreatorCheck(beer, user);
        doNothing().when(beerValidationService).validateNameDuplication(createBeerDto);

        when(beerRepository.save(beer)).thenReturn(beer);
        //Act
        Beer test = beerServiceActions.update(createBeerDto, user);
        // Assert
        assertEquals(beer, test);
    }

    @Test
    public void tagBeer_Should_Succeed() {
        //arrange
        User user = createTestUser();
        Beer beer = createTestBeer(user);
        Tag tag = createTestTag();

        when(tagService.create(tag, user)).thenReturn(tag);
        when(beerRepository.save(beer)).thenReturn(beer);
        //act
        Beer updated = beerServiceActions.tagBeer(beer, tag, user);
        //assert
        assertTrue(updated.getTags().contains(tag));
    }

    @Test
    public void rateBeer_Should_Succeed() {
        //arrange
        User user = createTestUser();
        Beer beer = createTestBeer(user);
        Rating rating = createTestRating();

        when(beerRepository.save(beer)).thenReturn(beer);
        //act
        Beer updated = beerServiceActions.rateBeer(beer, rating, user);
        //assert
        assertTrue(updated.getRatings().contains(rating));
    }

    @Test
    public void removeRating_Should_Succeed() {
        //arrange
        User user = createTestUser();
        Beer beer = createTestBeer(user);
        Rating rating = createTestRating();

        when(beerRepository.save(beer)).thenReturn(beer);
        //act
        Beer updated = beerServiceActions.rateBeer(beer, rating, user);
        //assert
        assertTrue(updated.getRatings().contains(rating));
    }
}
