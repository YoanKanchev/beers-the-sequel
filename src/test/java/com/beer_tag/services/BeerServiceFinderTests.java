package com.beer_tag.services;

import com.beer_tag.exceptions.AuthorisationException;
import com.beer_tag.exceptions.EntityNotFoundException;
import com.beer_tag.heplers.FilterBuilder;
import com.beer_tag.heplers.specifications.FilterSpecification;
import com.beer_tag.heplers.specifications.impl.BeerFilterSpecificationImpl;
import com.beer_tag.models.dto.beers.BeerFilterDto;
import com.beer_tag.models.dto.beers.CreateBeerDto;
import com.beer_tag.models.impl.Beer;
import com.beer_tag.models.impl.Brewery;
import com.beer_tag.models.impl.Style;
import com.beer_tag.models.impl.User;
import com.beer_tag.repositories.BeerRepository;
import com.beer_tag.repositories.TagRepository;
import com.beer_tag.services.mapping.ModelMapperService;
import com.beer_tag.services.validation.BaseValidationsService;
import com.beer_tag.services.validation.BeerValidationService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Sort;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import static com.beer_tag.TestFactory.createTestBeer;
import static com.beer_tag.TestFactory.createTestBrewery;
import static com.beer_tag.TestFactory.createTestCreateBeerDto;
import static com.beer_tag.TestFactory.createTestStyle;
import static com.beer_tag.TestFactory.createTestUser;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anySet;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class BeerServiceFinderTests {

		@InjectMocks
		BeerServiceFinderImpl beerServiceFinder;
		//		@InjectMocks
		//		TagServiceImpl        tagService;

		@Mock
		BeerRepository         beerRepository;
		@Mock
		BreweryService         breweryService;
		@Mock
		StyleService           styleService;
		@Mock
		CountryService         countryService;
		@Mock
		TagRepository          tagRepository;
		@Mock
		ModelMapperService     modelMapper;
		@Mock
		BeerValidationService  beerValidationService;
		@Mock
		BaseValidationsService baseValidationsService;
		@Mock
		FilterBuilder          filterBuilder;

		@Test
		public void getById_Should_Succeed() {
				//Arrange
				int id = 1;
				Beer beer = new Beer();
				beer.setId(id);

				when(beerRepository.findById(id)).thenReturn(Optional.of(beer));
				//Act
				Beer beerTest = beerServiceFinder.getById(id);
				// Assert
				assertEquals(id, beerTest.getId());
		}

		@Test
		public void getById_Should_Fail_when_Not_Existing() {
				//Arrange
				int id = 1;
				Beer beer = new Beer();
				beer.setId(id);

				when(beerRepository.findById(id)).thenReturn(Optional.empty());
				//Act
				// Assert
				assertThrows(EntityNotFoundException.class, () -> beerServiceFinder.getById(id));
		}

		@Test
		public void getAll_Should_Succeed() {
				//Arrange
				ArrayList<Beer> beers = new ArrayList<>();
				when(beerRepository.findAll()).thenReturn(beers);
				//Act
				List<Beer> beersTest = beerServiceFinder.getAll();
				// Assert
				assertEquals(beers, beersTest);
		}

		@Test
		public void getByName_Should_Succeed() {
				//Arrange
				String name = anyString();
				Beer beer = new Beer();
				beer.setName(name);

				when(beerRepository.findByName(name)).thenReturn(Optional.of(beer));
				//Act
				Beer beerTest = beerServiceFinder.getByName(name);
				// Assert
				assertEquals(name, beerTest.getName());
		}

		@Test
		public void getName_Should_Fail_when_Not_Existing() {
				//Arrange
				String name = anyString();
				Beer beer = new Beer();
				beer.setName(name);

				when(beerRepository.findByName(name)).thenReturn(Optional.empty());
				//Act
				// Assert
				assertThrows(EntityNotFoundException.class, () -> beerServiceFinder.getByName(name));
		}

		@Test
		public void filterOne_Should_Succeed() {
				//Arrange
				Beer testBeer = new Beer();
				BeerFilterDto beerDto = new BeerFilterDto();
				FilterSpecification<Beer> beerFilterSpecification = new BeerFilterSpecificationImpl();

				when(filterBuilder.createSpecification(beerDto)).thenReturn(beerFilterSpecification);
				when(beerRepository.findOne(beerFilterSpecification)).thenReturn(Optional.of(testBeer));
				//Act
				Beer test = beerServiceFinder.filterOne(beerDto);
				//Assert
				assertEquals(testBeer, test);
		}

}
