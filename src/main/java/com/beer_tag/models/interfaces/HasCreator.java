package com.beer_tag.models.interfaces;

import com.beer_tag.models.impl.User;

public interface HasCreator {

		User getCreatedBy();

}
