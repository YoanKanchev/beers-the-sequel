package com.beer_tag.models.dto;

public interface FilterDto {
		boolean isSorted();

		boolean isOrderAscending();

		String getSortBy();
}
