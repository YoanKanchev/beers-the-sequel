package com.beer_tag.models.dto;

import lombok.Data;

@Data
public class EmailDto {
		private String email;
}
