package com.beer_tag.models.dto.users;

import com.beer_tag.heplers.anotations.PasswordValueMatch;
import com.beer_tag.heplers.anotations.ValidPassword;
import lombok.Getter;
import lombok.Setter;

@PasswordValueMatch.List({
		@PasswordValueMatch(
				field = "newPassword",
				fieldMatch = "confirmPassword",
				message = "Passwords do not match!"
		)
})
@Getter
@Setter
public class UpdateForgottenPasswordDto {

		private Integer id;

		private String validationToken;

		@ValidPassword
		private String newPassword;

		private String confirmPassword;

		public UpdateForgottenPasswordDto(Integer userId, String validationToken) {
				this.id = userId;
				this.validationToken = validationToken;
		}
}
