package com.beer_tag.models.dto.beers;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Getter
@Setter
public class CreateBreweryDto {
		private int id = -1;
		@Size(min = 4, max = 30, message = "Name must be between 4 and 30 characters!")
		private String name;

		@NotNull(message = "You need to pick a country!!")
		private Integer countryId;

		@Size(max = 200, message = "Description must be between 0 and 200 characters")
		private String shortDescription;

		private String moreInfoLink;

		private String picture;

		@NotNull
		private String foundedDate;
}
