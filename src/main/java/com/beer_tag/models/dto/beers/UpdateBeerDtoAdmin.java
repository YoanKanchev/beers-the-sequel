package com.beer_tag.models.dto.beers;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

@Getter
@Setter
public class UpdateBeerDtoAdmin {

		private int id = -1;

		@Size(min = 2, max = 20, message = "Beer name must be between 2 and 20 characters!")
		private String  name;
		@NotNull(message = "You need to specify a brewery!!")
		private Integer breweryId;
		@NotNull(message = "You need to pick a style!!")
		private Integer styleId;
		@Positive
		private Double  abv;

		private Integer createdById;

		@Size(max = 200, message = "Description must be between 0 and 200 characters")
		private String description;

		private Boolean pictureReset;

}
