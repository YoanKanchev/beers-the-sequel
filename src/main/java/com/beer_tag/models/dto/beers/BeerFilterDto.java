package com.beer_tag.models.dto.beers;

import com.beer_tag.models.dto.FilterDto;
import com.beer_tag.models.impl.Tag;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Collection;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class BeerFilterDto implements FilterDto {

		private Integer         id;
		private String          name;
		private Collection<Tag> tags;
		private Integer         countryId;
		private Integer         styleId;
		private Integer         breweryId;
		private String          sortBy;
		private Boolean         isActive;
		private boolean         orderAscending;


	public BeerFilterDto(Integer id, Boolean isActive) {
		this.id = id;
		this.isActive = isActive;
	}

	public BeerFilterDto(Boolean isActive) {
		this.isActive = isActive;
	}

	public boolean isSorted() {
				return sortBy != null && !sortBy.equals("-1");
		}
}
