package com.beer_tag.models.dto.beers;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

@Getter
@Setter
public class CreateRatingDto {

		private String  comment;
		private Integer userId;
		private Integer beerId;
		@Max(value = 5)
		@Positive
		@NotNull
		private Integer rating;
}
