package com.beer_tag.models.enums;

public enum Role implements Comparable<Role> {
		ROLE_USER(3),
		ROLE_AGENT(2),
		ROLE_ADMIN(1);

		private final int value;

		Role(int value) {
				this.value = value;
		}

		public int getValue() {
				return value;
		}
}
