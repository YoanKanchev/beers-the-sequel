package com.beer_tag.models.impl;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.util.Calendar;
import java.util.Date;

@Getter
@Setter
@Entity
@Table(name = "user_tokens")
public class UserToken {

		@Id
		@GeneratedValue(strategy = GenerationType.IDENTITY)
		@Column(name = "id")
		private int id;

		@Column(name = "token_value")
		private String tokenValue;

		@Column(name = "creation_date")
		private Date creationDate;

		@Column(name = "expiration_date")
		private Date expirationDate;

		@OneToOne()
		@JoinColumn(name = "user_id")
		private User user;

		public UserToken(String tokenValue, User user, Integer expirationTimeInHours) {
				this.user = user;
				creationDate = new Date();
				setExpirationDate(creationDate, expirationTimeInHours);
				this.tokenValue = tokenValue;
		}

		public UserToken() {
		}

		private void setExpirationDate(Date createDate, int expirationTimeInHours) {
				Calendar calendar = Calendar.getInstance();
				calendar.setTime(createDate);
				calendar.add(Calendar.HOUR, expirationTimeInHours);
				this.expirationDate = calendar.getTime();
		}

		public boolean isExpired() {
				long expirationDateMilliseconds = expirationDate.getTime();
				long currentDateMilliseconds = new Date().getTime();
				return currentDateMilliseconds > expirationDateMilliseconds;
		}

}
