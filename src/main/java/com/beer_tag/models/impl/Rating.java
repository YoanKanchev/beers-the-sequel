package com.beer_tag.models.impl;

import com.beer_tag.models.interfaces.HasCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Data
@Entity
@Table(name = "beer_ratings")
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class Rating implements Comparable<Rating>, HasCreator {

		@Id
		@EqualsAndHashCode.Include
		@GeneratedValue(strategy = GenerationType.IDENTITY)
		@Column(name = "id")
		private Integer id;

		@JsonIgnore
		@ManyToOne
//		@EqualsAndHashCode.Include
		@JoinColumn(name = "beer_id")
		private Beer beer;

		@JsonIgnore
		@ManyToOne
		//@EqualsAndHashCode.Include
		@JoinColumn(name = "user_id")
		private User createdBy;

		@Column(name = "rating")
		private Integer rating;

		@Column(name = "comment")
		private String comment;

		@Column(name = "create_date")
		private String createDate = LocalDateTime.now().toString();

		@Override
		public int compareTo(Rating o) {
				return rating.compareTo(o.getRating());
		}
}
