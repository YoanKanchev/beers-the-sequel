package com.beer_tag.models.impl;

import com.beer_tag.models.enums.Role;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.apache.logging.log4j.util.Strings;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityNotFoundException;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;
import java.util.stream.Collectors;

import static com.beer_tag.heplers.GlobalConstants.DEFAULT_PICTURE;


@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Entity
@Table(name = "users")
public class User implements UserDetails {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @EqualsAndHashCode.Include
    @Column(name = "id")
    private int id;

    @EqualsAndHashCode.Include
    @Column(name = "username")
    private String username;

    @Column(name = "password")
    private String password;


    @OneToMany(mappedBy = "user", fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<UserRole> authorities;

    @Column(name = "enabled")
    private boolean enabled;

    @JsonIgnore
    @Transient
    private boolean accountNonExpired = true;

    @JsonIgnore
    @Transient
    private boolean accountNonLocked = true;

    @JsonIgnore
    @Transient
    private boolean credentialsNonExpired = true;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "email")
    private String email;

    @Column(name = "picture")
    private String picture;

    @Column(name = "registration_date")
    private String registrationDate;

    @Column(name = "deactivation_date")
    private String deactivationDate;

    @Column(name = "last_edited_date")
    private String updateDate;


    @OneToMany(mappedBy = "createdBy", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private final Set<Beer> createdBeers = new HashSet<>();

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "drunk_beers",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "beer_id"))
    private final Set<Beer> drunkBeers = new HashSet<>();

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "favourite_beers",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "beer_id"))
    private final Set<Beer> favouriteBeers = new HashSet<>();

    @OneToMany(mappedBy = "createdBy", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private final Set<Rating> givenRatings = new HashSet<>();

    public void addBeerToDrunk(Beer beer) {
        drunkBeers.add(beer);
    }

    public void removeBeerFromDrunk(Beer beer) {
        drunkBeers.removeIf(theBeer -> theBeer.getId() == (beer.getId()));
    }

    public void addBeerToFavourite(Beer beer) {
        favouriteBeers.add(beer);
    }

    public void removeBeerFromFavourite(Beer beer) {
        favouriteBeers.removeIf(theBeer -> theBeer.getId() == (beer.getId()));
    }

    public boolean hasFavouriteBeer(int beerId) {
        Beer beer = new Beer();
        beer.setId(beerId);
        return favouriteBeers.contains(beer);
    }

    public boolean hasDrunkBeer(int beerId) {
        Beer beer = new Beer();
        beer.setId(beerId);
        return drunkBeers.contains(beer);
    }

    public boolean hasRatedBeer(int beerId) {
        return givenRatings.stream().anyMatch(rating -> rating.getBeer().getId() == beerId);
    }

    public boolean hasRole(String role) {
        return authorities.contains(new UserRole(role));
    }

    public void deactivate() {
        setEnabled(false);
        setDeactivationDate(LocalDate.now().toString());
    }

    public void activate() {
        setEnabled(true);
    }

    public void addAuthorities(UserRole... authority) {
        authorities.addAll(Arrays.asList(authority));
    }

    public void removeAuthorities(UserRole... authority) {
        authorities.removeAll(Arrays.asList(authority));
    }

    public void removeRating(Rating rating) {
        if (!givenRatings.remove(rating)) {
            throw new EntityNotFoundException("Invalid rating");
        }
    }

    public ArrayList<String> getAuthoritiesList() {
        return authorities.stream().map(UserRole::getAuthority).collect(Collectors.toCollection(ArrayList::new));
    }

    public String getHighestAuthority() {
        LinkedList<Role> authorities = this.authorities.stream()
                .map(role -> Role.valueOf(role.getAuthority())).sorted(Comparator.comparingInt(Role::getValue))
                .collect(Collectors.toCollection(LinkedList::new));

        return authorities.getFirst().toString();
    }

    public void setPicture(String picture) {
        this.picture = Strings.isEmpty(picture) ? DEFAULT_PICTURE : picture;
    }
}


