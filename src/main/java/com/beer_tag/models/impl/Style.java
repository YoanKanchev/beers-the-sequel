package com.beer_tag.models.impl;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "styles")
public class Style {

		@Id
		@GeneratedValue(strategy = GenerationType.IDENTITY)
		@Column(name = "id")
		private int    id;
		@Column(name = "style")
		private String name;
		@Column(name = "description")
		private String description;
		@Column(name = "ABV")
		private String abv;
		@Column(name = "IBU")
		private String ibu;
		@Column(name = "glassware")
		private String glassware;
}
