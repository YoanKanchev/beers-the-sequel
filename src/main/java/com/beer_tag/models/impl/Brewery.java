package com.beer_tag.models.impl;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.logging.log4j.util.Strings;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import static com.beer_tag.heplers.GlobalConstants.DEFAULT_PICTURE;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "breweries")
public class Brewery {

		@Id
		@GeneratedValue(strategy = GenerationType.IDENTITY)
		@Column(name = "id")
		private Integer    id;

		@Column(name = "name")
		private String name;

		@JsonIgnore
		@ManyToOne
		@JoinColumn(name = "country_id")
		private Country country;

		@Column(name = "short_description")
		private String shortDescription;

		@Column(name = "more_info_link")
		private String moreInfoLink;

		@Column(name = "picture")
		private String picture;

		@Column(name = "founded_date")
		private String foundedDate;

		public void setPicture(String picture) {
				this.picture = Strings.isEmpty(picture) ? DEFAULT_PICTURE : picture;
		}

		public Integer getId() {
				return id;
		}
}

