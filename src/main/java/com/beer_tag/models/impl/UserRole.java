package com.beer_tag.models.impl;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.springframework.security.core.GrantedAuthority;

import javax.persistence.*;
import java.io.Serializable;

@NoArgsConstructor
@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Entity
@Table(name = "user_roles")
public class UserRole implements GrantedAuthority {

    @Id
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    @Id
    @EqualsAndHashCode.Include
    @Column(name = "authority")
    private String authority;

    public UserRole(String authority) {
        this.authority = authority;
    }

    public UserRole(User user, String authority) {
        this.authority = authority;
        this.user = user;
    }
}
