package com.beer_tag.models.impl;

import com.beer_tag.models.interfaces.HasCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.apache.logging.log4j.util.Strings;
import org.hibernate.annotations.Formula;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import static com.beer_tag.heplers.GlobalConstants.DEFAULT_PICTURE;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "beers")
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class Beer implements HasCreator, Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @EqualsAndHashCode.Include
    @Column(name = "id")
    private int id;

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

    @ManyToOne
    @JoinColumn(name = "brewery_id")
    private Brewery brewery;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "beer_tags", joinColumns = @JoinColumn(name = "beer_id"), inverseJoinColumns = @JoinColumn(name = "tag_id"))
    private Set<Tag> tags = new HashSet<>();

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "style_id")
    private Style style;

    @Column(name = "abv")
    private double abv;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "created_by")
    private User createdBy;

    @Column(name = "create_date")
    private String creationDate;

    @Column(name = "last_edited_date")
    private String lastEditedDate = creationDate;

    @Column(name = "deactivation_date")
    private String deactivationDate = null;

    @Column(name = "active")
    private Boolean active = true;

    @Column(name = "picture")
    private String picture;

    @OneToMany(mappedBy = "beer", fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<Rating> ratings;

    @Formula("(SELECT AVG(br.rating) from beer_ratings br where br.beer_id = id)")
    private Double avgRating;

    public void addTag(Tag tag) {
        tags.add(tag);
    }

    public void addRating(Rating rating) {
        ratings.add(rating);
    }

    public void removeRating(Rating rating) {
        ratings.remove(rating);
    }

    public void deactivate() {
        setActive(false);
        setLastEditedDate(LocalDateTime.now().toString());
        setDeactivationDate(LocalDateTime.now().toString());
    }

    public void activate() {
        setActive(true);
        setLastEditedDate(LocalDateTime.now().toString());
        setDeactivationDate(null);
    }

    public void setPicture(String picture) {
        this.picture = Strings.isEmpty(picture) ? DEFAULT_PICTURE : picture;
    }

    public Country getCountry() {
        return brewery.getCountry();
    }
}
