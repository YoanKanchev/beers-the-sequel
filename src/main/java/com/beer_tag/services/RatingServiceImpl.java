package com.beer_tag.services;

import com.beer_tag.exceptions.EntityNotFoundException;
import com.beer_tag.models.impl.Rating;
import com.beer_tag.repositories.RatingRepository;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.beer_tag.heplers.GlobalConstants.*;

@Service
public class RatingServiceImpl implements RatingService {

    private final RatingRepository ratingRepository;

    public RatingServiceImpl(RatingRepository ratingRepository) {
        this.ratingRepository = ratingRepository;
    }


    @Override
    public Rating getById(int id) {
        return ratingRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException(String.format(RATING_ENTITY + WITH_ID + NOT_FOUND, id)));
    }

    @Override
    public List<Rating> getAll() {
        return ratingRepository.findAll();
    }
}
