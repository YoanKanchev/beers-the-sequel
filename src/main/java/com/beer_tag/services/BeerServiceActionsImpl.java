package com.beer_tag.services;

import com.beer_tag.models.dto.beers.CreateBeerDto;
import com.beer_tag.models.impl.Beer;
import com.beer_tag.models.impl.Brewery;
import com.beer_tag.models.impl.Rating;
import com.beer_tag.models.impl.Style;
import com.beer_tag.models.impl.Tag;
import com.beer_tag.models.impl.User;
import com.beer_tag.repositories.BeerRepository;
import com.beer_tag.repositories.RatingRepository;
import com.beer_tag.services.mapping.ModelMapperService;
import com.beer_tag.services.validation.BaseValidationsService;
import com.beer_tag.services.validation.BeerValidationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
public class BeerServiceActionsImpl implements BeerServiceActions {

		private final BeerRepository         beerRepository;
		private final RatingRepository       ratingRepository;
		private final TagService             tagService;
		private final ModelMapperService     modelMapperService;
		private final BeerValidationService  beerValidationService;
		private final BaseValidationsService baseValidationsService;
		private final BreweryService         breweryService;
		private final StyleService           styleService;
		private final BeerServiceFinder      beerServiceFinder;

		@Autowired
		public BeerServiceActionsImpl(BeerRepository beerRepository, TagService tagService, ModelMapperService modelMapperService,
				BeerValidationService beerValidationService, BaseValidationsService baseValidationsService,
				BreweryService breweryService, StyleService styleService, RatingRepository ratingRepository,
				BeerServiceFinder beerServiceFinder) {
				this.beerRepository = beerRepository;
				this.tagService = tagService;
				this.modelMapperService = modelMapperService;
				this.beerValidationService = beerValidationService;
				this.baseValidationsService = baseValidationsService;
				this.breweryService = breweryService;
				this.styleService = styleService;
				this.ratingRepository = ratingRepository;
				this.beerServiceFinder = beerServiceFinder;
		}

		@Override
		public Beer create(CreateBeerDto beerDto, User loggedUser) {
				beerValidationService.validateNameDuplication(beerDto);
				Style style = styleService.getById(beerDto.getStyleId());
				Brewery brewery = breweryService.getById(beerDto.getBreweryId());
				Beer beer = modelMapperService.toBeer(beerDto, loggedUser, style, brewery);
				beer.setActive(true);
				beerRepository.save(beer);
				return beer;
		}

		@Override
		public void activate(int beerId, User loggedUser) {
				Beer beer = beerServiceFinder.getById(beerId);
				baseValidationsService.adminORCreatorCheck(beer, loggedUser);
				beer.activate();
				beerRepository.save(beer);
		}

		@Override
		public void deactivate(int beerId, User loggedUser) {
				Beer beer = beerServiceFinder.getById(beerId);
				baseValidationsService.adminORCreatorCheck(beer, loggedUser);
				beer.deactivate();
				beerRepository.save(beer);
		}

		@Override
		public Beer update(CreateBeerDto beerDto, User loggedUser) {
				Beer beerToUpdate = beerServiceFinder.getById(beerDto.getId());
				baseValidationsService.adminORCreatorCheck(beerToUpdate, loggedUser);
				beerValidationService.validateNameDuplication(beerDto);
				Style style = styleService.getById(beerDto.getStyleId());
				Brewery brewery = breweryService.getById(beerDto.getBreweryId());
				Beer updatedBeer = modelMapperService.toBeer(beerDto, beerToUpdate, style, brewery);
				return beerRepository.save(updatedBeer);
		}

		@Override
		public Beer tagBeer(Beer beer, Tag tag, User loggedUser) {
				tag = tagService.create(tag, loggedUser);
				beer.addTag(tag);
				return beerRepository.save(beer);
		}

		@Override
		public Beer rateBeer(Beer beer, Rating rating, User loggedUser) {
				beer.addRating(rating);
				return beerRepository.save(beer);
		}

		@Transactional
		@Override
		public void removeRating(int ratingId, Beer beer, User loggedUser) {
				Rating rating = ratingRepository.getOne(ratingId);
				baseValidationsService.adminORCreatorCheck(rating, loggedUser);
				rating.getCreatedBy().removeRating(rating);
				beer.removeRating(rating);
//				ratingRepository.delete(rating);
		}
}
