package com.beer_tag.services;

import com.beer_tag.exceptions.EntityNotFoundException;
import com.beer_tag.models.impl.Country;
import com.beer_tag.repositories.CountryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.beer_tag.heplers.GlobalConstants.*;

@Service
public class CountryServiceImpl implements CountryService {

    private final CountryRepository countryRepository;

    @Autowired
    public CountryServiceImpl(CountryRepository countryRepository) {
        this.countryRepository = countryRepository;
    }

    @Override
    public Country getById(int id) {
        return countryRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException(String.format(COUNTRY_ENTITY + WITH_ID + NOT_FOUND, id)));
    }

    @Override
    public List<Country> getAll() {
        return countryRepository.findAll();
    }
}
