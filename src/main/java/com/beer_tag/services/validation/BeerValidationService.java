package com.beer_tag.services.validation;

import com.beer_tag.models.dto.beers.CreateBeerDto;

public interface BeerValidationService {

		void validateNameDuplication(CreateBeerDto beer);
}
