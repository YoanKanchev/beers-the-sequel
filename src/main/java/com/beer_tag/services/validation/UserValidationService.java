package com.beer_tag.services.validation;

import com.beer_tag.models.impl.User;
import com.beer_tag.models.impl.UserToken;

public interface UserValidationService {

		void roleAuthorisationValidation(User loggedUser, String role);

		void validateUniqueUserName(String name);

		void validateUniqueEmail(String email);

		void validateUniqueUserName(String username, Integer userId);

		void validateUniqueEmail(String email, Integer userId);

		void validateCorrectPassword(String password, String realPassword);

		void validateAdmin(User loggedUser);

		void validateToken(User user, UserToken token);
}
