package com.beer_tag.services.validation;

import com.beer_tag.exceptions.DuplicateEntityException;
import com.beer_tag.models.dto.beers.CreateBeerDto;
import com.beer_tag.models.impl.Beer;
import com.beer_tag.repositories.BeerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

import static com.beer_tag.heplers.GlobalConstants.*;

@Service
public class BeerValidationServiceImpl implements BeerValidationService {

    private final BeerRepository beerRepository;

    @Autowired
    public BeerValidationServiceImpl(BeerRepository beerRepository) {
        this.beerRepository = beerRepository;
    }

    @Override
    public void validateNameDuplication(CreateBeerDto beer) {
        Optional<Beer> presentBeer = beerRepository.findByName(beer.getName());
        if ((presentBeer.isPresent()) && (presentBeer.get().getId() != beer.getId())) {
            throw new DuplicateEntityException(String.format(BEER_ENTITY + WITH_NAME + ALREADY_EXISTS, beer.getName()));
        }
    }
}
