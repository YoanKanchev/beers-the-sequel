package com.beer_tag.services.validation;

import com.beer_tag.exceptions.DuplicateEntityException;
import com.beer_tag.models.dto.beers.CreateBreweryDto;
import com.beer_tag.models.impl.Brewery;
import com.beer_tag.repositories.BreweryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

import static com.beer_tag.heplers.GlobalConstants.*;

@Service
public class BreweryValidationServiceImpl implements BreweryValidationService {

    private final BreweryRepository breweryRepository;

    @Autowired
    public BreweryValidationServiceImpl(BreweryRepository breweryRepository) {
        this.breweryRepository = breweryRepository;
    }

    @Override
    public void validateNameDuplication(CreateBreweryDto createData) {
        Optional<Brewery> presentBrewery = breweryRepository.findByName(createData.getName());
        if ((presentBrewery.isPresent()) && (presentBrewery.get().getId() != createData.getId())) {
            throw new DuplicateEntityException(String.format(BREWERY + WITH_NAME + ALREADY_EXISTS, createData.getName()));
        }
    }
}

