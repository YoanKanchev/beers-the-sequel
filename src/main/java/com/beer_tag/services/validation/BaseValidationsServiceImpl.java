package com.beer_tag.services.validation;

import com.beer_tag.exceptions.AuthorisationException;
import com.beer_tag.exceptions.EntityNotFoundException;
import com.beer_tag.models.impl.User;
import com.beer_tag.models.interfaces.HasCreator;
import org.springframework.stereotype.Service;

import java.util.Arrays;

import static com.beer_tag.heplers.GlobalConstants.ROLE_ADMIN;
import static com.beer_tag.heplers.GlobalConstants.INVALID_ROLE;
import static com.beer_tag.heplers.GlobalConstants.YOU_ARE_UNAUTHORISED;

@Service
public class BaseValidationsServiceImpl implements BaseValidationsService {

		@Override
		public void adminORCreatorCheck(HasCreator entity, User loggedUser) {
				if ((!loggedUser.isEnabled())
						|| (!loggedUser.hasRole(ROLE_ADMIN)
						&& !loggedUser.equals(entity.getCreatedBy()))) {
						throw new AuthorisationException(YOU_ARE_UNAUTHORISED);
				}
		}

		@Override
		public <T extends Enum<T>> void validateEnum(String toValidate, Class<T> enumerator) {
				Arrays.stream(enumerator.getEnumConstants()).filter(x -> x.toString().equals(toValidate)).findAny()
						.orElseThrow(() -> new EntityNotFoundException(INVALID_ROLE));
		}
}
