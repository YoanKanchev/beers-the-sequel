package com.beer_tag.services.validation;

import com.beer_tag.models.dto.beers.CreateBreweryDto;

public interface BreweryValidationService {

		void validateNameDuplication(CreateBreweryDto createData);
}
