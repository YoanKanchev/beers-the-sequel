package com.beer_tag.services.validation;

import com.beer_tag.models.impl.User;
import com.beer_tag.models.interfaces.HasCreator;
import org.springframework.stereotype.Service;

public interface BaseValidationsService {

		void adminORCreatorCheck(HasCreator entity, User loggedUser);

		<T extends Enum<T>> void validateEnum(String toValidate, Class<T> enumerator);
}
