package com.beer_tag.services;

import com.beer_tag.models.impl.Tag;
import com.beer_tag.models.impl.User;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.Set;

public interface TagService {

		Tag create(Tag tag, User user);

		void delete(Tag tag, User user);

		Tag getById(int id);

		Optional<Tag> getByName(String tagName);

		List<Tag> getAll();

		Collection<Tag> getAllFromCollectionOfNames(Set<String> names);

}
