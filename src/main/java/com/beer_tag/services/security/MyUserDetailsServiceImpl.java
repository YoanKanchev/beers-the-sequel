package com.beer_tag.services.security;

import com.beer_tag.repositories.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class MyUserDetailsServiceImpl implements MyUserDetailsService {

		private final UserRepository userRepository;

		@Autowired
		public MyUserDetailsServiceImpl(UserRepository userRepository) {
				this.userRepository = userRepository;
		}

		@Override
		public UserDetails loadUserByUsername(String username) {
				return userRepository.findByUsername(username)
						.orElseThrow(() -> new UsernameNotFoundException(username));
		}
}