package com.beer_tag.services.security;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;

public interface MyUserDetailsService extends UserDetailsService {

		@Override
		UserDetails loadUserByUsername(String username);
}
