package com.beer_tag.services;

import com.beer_tag.exceptions.DuplicateEntityException;
import com.beer_tag.exceptions.EntityNotFoundException;
import com.beer_tag.models.impl.Style;
import com.beer_tag.models.impl.User;
import com.beer_tag.repositories.StyleRepository;
import com.beer_tag.services.validation.UserValidationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

import static com.beer_tag.heplers.GlobalConstants.*;

@Service
public class StyleServiceImpl implements StyleService {

    private final StyleRepository repository;
    private final UserValidationService userValidationService;

    @Autowired
    public StyleServiceImpl(StyleRepository repository, UserValidationService userValidationService) {
        this.repository = repository;
        this.userValidationService = userValidationService;
    }

    @Override
    public List<Style> getAll() {
        return repository.findAll();
    }

    @Override
    public void create(Style style) {
        validateNameDuplication(style);
        repository.save(style);
    }

    @Override
    public Style update(Style style, User loggedUser) {
        userValidationService.roleAuthorisationValidation(loggedUser, ROLE_ADMIN);
        validateNameDuplication(style);
        return repository.save(style);
    }

    @Override
    public Style getById(int id) {
        return repository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException(String.format(STYLE_ENTITY + WITH_ID + NOT_FOUND, id)));
    }

    @Override
    public void delete(int id, User loggedUser) {
        userValidationService.roleAuthorisationValidation(loggedUser, ROLE_ADMIN);
        Style style = getById(id);
        repository.delete(style);
    }

    private void validateNameDuplication(Style style) {
        Optional<Style> present = repository.findByName(style.getName());
        if ((present.isPresent()) && (present.get().getId() != style.getId())) {
            throw new DuplicateEntityException(String.format(STYLE_ENTITY + WITH_NAME + ALREADY_EXISTS, style.getName()));
        }
    }

}
