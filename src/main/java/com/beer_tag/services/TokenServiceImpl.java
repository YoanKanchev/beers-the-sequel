package com.beer_tag.services;

import com.beer_tag.exceptions.EntityNotFoundException;
import com.beer_tag.models.impl.User;
import com.beer_tag.models.impl.UserToken;
import com.beer_tag.repositories.TokenRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

import static com.beer_tag.heplers.GlobalConstants.NOT_FOUND;
import static com.beer_tag.heplers.GlobalConstants.TOKEN_ENTITY;

@Service
public class TokenServiceImpl implements TokenService {

    public static final int EXPIRATION_TIME_IN_HOURS = 1;
    private final TokenRepository tokenRepository;

    @Autowired
    public TokenServiceImpl(TokenRepository tokenRepository) {
        this.tokenRepository = tokenRepository;
    }

    @Override
    public List<UserToken> getAll() {
        return tokenRepository.findAll();
    }

    @Override
    public UserToken searchByName(String tokenValue) {
        return tokenRepository.findByTokenValue(tokenValue)
                .orElseThrow(() -> new EntityNotFoundException(TOKEN_ENTITY + NOT_FOUND));
    }

    @Override
    public UserToken searchById(int id) {
        return tokenRepository.getOne(id);
    }

    @Override
    public void delete(int id, User loggedUser) {
        UserToken token = searchById(id);
        tokenRepository.delete(token);
    }

    @Override
    public UserToken newToken(User user) {
        UserToken token = new UserToken(generateTokenValue(), user, EXPIRATION_TIME_IN_HOURS);
        return tokenRepository.save(token);
    }

    private String generateTokenValue() {
        String token = UUID.randomUUID().toString();
        while (tokenRepository.findByTokenValue(token).isPresent()) {
            token = UUID.randomUUID().toString();
        }
        return token;
    }
}
