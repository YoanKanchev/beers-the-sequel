package com.beer_tag.services;

import com.beer_tag.exceptions.EntityNotFoundException;
import com.beer_tag.models.dto.beers.CreateBreweryDto;
import com.beer_tag.models.impl.Brewery;
import com.beer_tag.models.impl.Country;
import com.beer_tag.models.impl.User;
import com.beer_tag.repositories.BreweryRepository;
import com.beer_tag.services.mapping.ModelMapperService;
import com.beer_tag.services.validation.BreweryValidationService;
import com.beer_tag.services.validation.UserValidationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.beer_tag.heplers.GlobalConstants.*;
import static com.beer_tag.models.enums.Role.ROLE_ADMIN;

@Service
public class BreweryServiceImpl implements BreweryService {

    private final BreweryRepository breweryRepository;
    private final UserValidationService userValidationService;
    private final BreweryValidationService breweryValidationService;
    private final ModelMapperService modelMapperService;
    private final CountryService countryService;

    @Autowired
    public BreweryServiceImpl(BreweryRepository breweryRepository, UserValidationService userValidationService,
                              BreweryValidationService breweryValidationService, ModelMapperService modelMapperService,
                              CountryService countryService) {
        this.breweryRepository = breweryRepository;
        this.userValidationService = userValidationService;
        this.breweryValidationService = breweryValidationService;
        this.modelMapperService = modelMapperService;
        this.countryService = countryService;
    }

    @Override
    public List<Brewery> getAll() {
        return breweryRepository.findAll();
    }

    @Override
    public List<Brewery> findAllByCountry(Country country) {
        return breweryRepository.findAllByCountry(country);
    }

    @Override
    public Brewery getByName(String name) {
        return breweryRepository.findByName(name)
                .orElseThrow(() -> new EntityNotFoundException(String.format(BREWERY + WITH_NAME + NOT_FOUND, name)));
    }

    @Override
    public Brewery getById(int id) {
        return breweryRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException(String.format(BREWERY + WITH_ID, id)));
    }

    @Override
    public void delete(int id, User loggedUser) {
        userValidationService.roleAuthorisationValidation(loggedUser, ROLE_ADMIN.toString());
        Brewery brewery = getById(id);
        breweryRepository.delete(brewery);
    }

    @Override
    public Brewery create(CreateBreweryDto createData) {
        breweryValidationService.validateNameDuplication(createData);
        Country country = countryService.getById(createData.getId());
        Brewery brewery = modelMapperService.toBrewery(createData, country);
        return breweryRepository.save(brewery);
    }

    @Override
    public Brewery update(CreateBreweryDto updateData, User loggedUser) {
        userValidationService.roleAuthorisationValidation(loggedUser, ROLE_ADMIN.toString());
        Brewery breweryToUpdate = getById(updateData.getId());
        Country country = countryService.getById(updateData.getId());
        breweryValidationService.validateNameDuplication(updateData);
        Brewery updatedBrewery = modelMapperService.toBrewery(updateData, breweryToUpdate, country);
        return breweryRepository.save(updatedBrewery);
    }
}
