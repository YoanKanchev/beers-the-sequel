package com.beer_tag.services;

import com.beer_tag.models.impl.Country;

import java.util.List;

public interface CountryService {

		Country getById(int id);

		List<Country> getAll();
}
