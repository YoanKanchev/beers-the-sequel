package com.beer_tag.services;

import com.beer_tag.models.dto.beers.CreateBreweryDto;
import com.beer_tag.models.impl.Brewery;
import com.beer_tag.models.impl.Country;
import com.beer_tag.models.impl.User;

import java.util.List;

public interface BreweryService {

		List<Brewery> getAll();

		Brewery getById(int id);

		List<Brewery> findAllByCountry(Country country);

		Brewery getByName(String name);

		Brewery create(CreateBreweryDto createData);

		Brewery update(CreateBreweryDto updateData, User loggedUser);

		void delete(int id, User loggedUser);
}
