package com.beer_tag.services;

import com.beer_tag.models.impl.Rating;

import java.util.List;

public interface RatingService {

		Rating getById(int id);

		List<Rating> getAll();
}
