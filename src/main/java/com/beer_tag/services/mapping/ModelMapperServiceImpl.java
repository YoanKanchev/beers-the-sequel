package com.beer_tag.services.mapping;

import com.beer_tag.models.dto.beers.CreateBeerDto;
import com.beer_tag.models.dto.beers.CreateBreweryDto;
import com.beer_tag.models.dto.beers.CreateRatingDto;
import com.beer_tag.models.dto.users.CreateUserDto;
import com.beer_tag.models.dto.users.UpdateUserDto;
import com.beer_tag.models.dto.users.UpdateUserFromAdminDto;
import com.beer_tag.models.impl.Beer;
import com.beer_tag.models.impl.Brewery;
import com.beer_tag.models.impl.Country;
import com.beer_tag.models.impl.Rating;
import com.beer_tag.models.impl.Style;
import com.beer_tag.models.impl.User;
import com.beer_tag.models.impl.UserRole;
import org.apache.logging.log4j.util.Strings;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.time.LocalDate;

@Component
public class ModelMapperServiceImpl implements ModelMapperService {

    @Override
    public Beer toBeer(CreateBeerDto beer, Beer beerToUpdate, Style style, Brewery brewery) {
        beerToUpdate.setName(beer.getName());
        beerToUpdate.setAbv(beer.getAbv());
        beerToUpdate.setDescription(beer.getDescription());
        beerToUpdate.setPicture(beer.getPicture());
        beerToUpdate.setStyle(style);
        beerToUpdate.setBrewery(brewery);
        return beerToUpdate;
    }

    @Override
    public Beer toBeer(CreateBeerDto beer, User user, Style style, Brewery brewery) {
        Beer beerToCreate = toBeer(beer, new Beer(), style, brewery);
        beerToCreate.setCreatedBy(user);
        return beerToCreate;
    }

    @Override
    public Rating toRating(CreateRatingDto ratingDto, Beer beer, User loggedUser) {
        Rating rating = new Rating();
        rating.setBeer(beer);
        rating.setCreatedBy(loggedUser);
        rating.setComment(ratingDto.getComment());
        rating.setRating(ratingDto.getRating());
        return rating;
    }

    @Override
    public Brewery toBrewery(CreateBreweryDto createData, Country country) {
        return toBrewery(createData, new Brewery(), country);
    }

    @Override
    public Brewery toBrewery(CreateBreweryDto createData, Brewery breweryToUpdate, Country country) {
        breweryToUpdate.setName(createData.getName());
        breweryToUpdate.setCountry(country);
        breweryToUpdate.setPicture(createData.getPicture());
        breweryToUpdate.setShortDescription(createData.getShortDescription());
        breweryToUpdate.setMoreInfoLink(createData.getMoreInfoLink());
        breweryToUpdate.setFoundedDate(createData.getFoundedDate());
        return breweryToUpdate;
    }

    @Override
    public User toUser(CreateUserDto userDto) {
        User user = new User();
        UserRole userRole = new UserRole(user, userDto.getRole());
        user.addAuthorities(userRole);
        user.setUsername(userDto.getUsername());
        user.setPassword(userDto.getPassword());
        user.setEmail(userDto.getEmail());
        user.setFirstName(userDto.getFirstName());
        user.setLastName(userDto.getLastName());
        user.setPicture(userDto.getPicture());
        return user;
    }

    @Override
    public User toUser(UpdateUserDto updateData, User userToUpdate) {
        userToUpdate.setEmail(updateData.getEmail());
        userToUpdate.setFirstName(updateData.getFirstName());
        userToUpdate.setLastName(updateData.getLastName());
        userToUpdate.setPicture(updateData.getPicture());
        userToUpdate.setUpdateDate(LocalDate.now().toString());
        return userToUpdate;
    }

    @Transactional
    @Override
    public User toUser(UpdateUserFromAdminDto updateData, User userToUpdate) {
        userToUpdate.setFirstName(updateData.getFirstName());
        userToUpdate.setLastName(updateData.getLastName());
        userToUpdate.setEmail(userToUpdate.getEmail());
        userToUpdate.setPicture(userToUpdate.getPicture());
        userToUpdate.setUpdateDate(LocalDate.now().toString());

        if (Strings.isNotEmpty(updateData.getAddRole())) {
            UserRole role = new UserRole(userToUpdate, updateData.getAddRole());
            userToUpdate.addAuthorities(role);
        }
        if (Strings.isNotEmpty(updateData.getRemoveRole())) {
            userToUpdate.removeAuthorities(new UserRole(updateData.getRemoveRole()));
        }
        return userToUpdate;
    }

    @Override
    public void toStyle(Style style, Style currentStyle) {
        String newName = style.getName();
        currentStyle.setName(newName);

        String newDescription = style.getDescription();
        currentStyle.setDescription(newDescription);

        String newABV = style.getAbv();
        currentStyle.setDescription(newABV);

        String newIBU = style.getIbu();
        currentStyle.setDescription(newIBU);

        String newGlassware = style.getGlassware();
        currentStyle.setDescription(newGlassware);
    }

}
