package com.beer_tag.services.mapping;

import com.beer_tag.models.dto.beers.CreateBeerDto;
import com.beer_tag.models.dto.beers.CreateBreweryDto;
import com.beer_tag.models.dto.beers.CreateRatingDto;
import com.beer_tag.models.dto.users.CreateUserDto;
import com.beer_tag.models.dto.users.UpdateUserDto;
import com.beer_tag.models.dto.users.UpdateUserFromAdminDto;
import com.beer_tag.models.impl.*;

public interface ModelMapperService {

    Beer toBeer(CreateBeerDto beer, Beer beerToUpdate, Style style, Brewery brewery);

    Beer toBeer(CreateBeerDto beer, User user, Style style, Brewery brewery);

    Rating toRating(CreateRatingDto ratingDto, Beer beer, User loggedUser);

    Brewery toBrewery(CreateBreweryDto createData, Country country);

    Brewery toBrewery(CreateBreweryDto updateData, Brewery breweryToUpdate, Country country);

    User toUser(CreateUserDto userDto);

    User toUser(UpdateUserDto updateData, User userToUpdate);

    User toUser(UpdateUserFromAdminDto updateData, User userToUpdate);

    void toStyle(Style style, Style currentStyle);
}
