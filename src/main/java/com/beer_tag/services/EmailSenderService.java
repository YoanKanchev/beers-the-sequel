package com.beer_tag.services;

import com.beer_tag.models.dto.EmailDtoHtml;
import com.beer_tag.models.dto.EmailDtoText;
import org.springframework.scheduling.annotation.Async;

import javax.mail.MessagingException;

public interface EmailSenderService {

		@Async
		void sendEmail(EmailDtoText emailDtoText);

		void sendHtmlEmailFromTemplate(EmailDtoHtml emailDtoHtml) throws MessagingException;

}
