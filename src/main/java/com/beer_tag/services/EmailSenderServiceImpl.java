package com.beer_tag.services;

import com.beer_tag.heplers.GlobalConstants;
import com.beer_tag.models.dto.EmailDtoHtml;
import com.beer_tag.models.dto.EmailDtoText;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring5.SpringTemplateEngine;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import static com.beer_tag.heplers.GlobalConstants.UTF_8;

@PropertySource("classpath:application.properties")
@Service
public class EmailSenderServiceImpl implements EmailSenderService {

    private final JavaMailSender mailSender;
    private final String from;
    private final SpringTemplateEngine thymeleafTemplateEngine;

    @Autowired
    public EmailSenderServiceImpl(JavaMailSender mailSender, Environment env, SpringTemplateEngine thymeleafTemplateEngine) {
        this.mailSender = mailSender;
        this.from = env.getProperty("spring.mail.username");
        this.thymeleafTemplateEngine = thymeleafTemplateEngine;
    }

    @Override
    @Async
    public void sendEmail(EmailDtoText emailDtoText) {
        SimpleMailMessage email = new SimpleMailMessage();
        email.setTo(emailDtoText.getTo());
        email.setSubject(emailDtoText.getSubject());
        email.setFrom(from);
        email.setText(emailDtoText.getMessage());
        mailSender.send(email);
    }

    @Override
    public void sendHtmlEmailFromTemplate(EmailDtoHtml emailDtoHtml) throws MessagingException {
        Context thymeleafContext = new Context();
        thymeleafContext.setVariables(emailDtoHtml.getTemplateModel());
        String htmlBody = thymeleafTemplateEngine.process(emailDtoHtml.getTemplate(), thymeleafContext);
        sendHtmlMessage(emailDtoHtml.getTo(), emailDtoHtml.getSubject(), htmlBody);
    }

    private void sendHtmlMessage(String to, String subject, String htmlBody) throws MessagingException {
        MimeMessage message = mailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message, true, UTF_8);
        helper.setTo(to);
        helper.setSubject(subject);
        helper.setText(htmlBody, true);
        mailSender.send(message);
    }

}
