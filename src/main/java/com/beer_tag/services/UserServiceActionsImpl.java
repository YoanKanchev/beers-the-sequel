package com.beer_tag.services;

import com.beer_tag.exceptions.AuthorisationException;
import com.beer_tag.models.dto.UpdatePasswordDto;
import com.beer_tag.models.dto.users.CreateUserDto;
import com.beer_tag.models.dto.users.DeleteUserDto;
import com.beer_tag.models.dto.EmailDtoHtml;
import com.beer_tag.models.dto.users.UpdateForgottenPasswordDto;

import com.beer_tag.models.dto.users.UpdateUserDto;
import com.beer_tag.models.impl.Beer;
import com.beer_tag.models.impl.User;
import com.beer_tag.models.impl.UserToken;
import com.beer_tag.repositories.UserRepository;
import com.beer_tag.services.mapping.ModelMapperService;
import com.beer_tag.services.validation.UserValidationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import java.time.LocalDate;
import java.util.Map;

import static com.beer_tag.heplers.GlobalConstants.*;

@Service
public class UserServiceActionsImpl implements UserServiceActions {

    private final UserRepository userRepository;
    private final UserServiceFinder userServiceFinder;
    private final PasswordEncoder passwordEncoder;
    private final ModelMapperService modelMapperService;
    private final UserValidationService userValidationService;
    private final EmailSenderService emailSenderService;
    private final TokenService tokenService;

    @Autowired
    public UserServiceActionsImpl(UserRepository userRepository, UserServiceFinder userServiceFinder, PasswordEncoder passwordEncoder,
                                  ModelMapperService modelMapperService, UserValidationService userValidationService, EmailSenderService emailSenderService,
                                  TokenService tokenService) {
        this.userRepository = userRepository;
        this.userServiceFinder = userServiceFinder;
        this.passwordEncoder = passwordEncoder;
        this.modelMapperService = modelMapperService;
        this.userValidationService = userValidationService;
        this.emailSenderService = emailSenderService;
        this.tokenService = tokenService;
    }

    @Override
    public void activate(int id, User loggedUser) {
        User user = userServiceFinder.getById(id);
        userValidationService.validateAdmin(loggedUser);

        user.activate();
        userRepository.save(user);
    }

    @Override
    public void activateWithToken(User user, UserToken token) {
        userValidationService.validateToken(user, token);
        user.activate();
        userRepository.save(user);
    }

    @Override
    public void deactivate(DeleteUserDto userDto, User loggedUser) {
        User user = userServiceFinder.getById(userDto.getId());

        if (!loggedUser.hasRole(ROLE_ADMIN))
            userValidationService.validateCorrectPassword(userDto.getPassword(), user.getPassword());

        user.deactivate();
        userRepository.save(user);
    }

    @Override
    public User createUser(CreateUserDto userDto) {
        userValidationService.validateUniqueUserName(userDto.getUsername());
        userValidationService.validateUniqueEmail(userDto.getEmail());
        String encodedPassword = passwordEncoder.encode(userDto.getPassword());
        User userToCreate = modelMapperService.toUser(userDto);
        userToCreate.setPassword(encodedPassword);

        return userRepository.save(userToCreate);
    }

    @Override
    public User update(UpdateUserDto updateData, User loggedUser) {
        User userToUpdate = userServiceFinder.getById(updateData.getId());

        if (!loggedUser.hasRole(ROLE_ADMIN))
            userValidationService.validateCorrectPassword(updateData.getPassword(), userToUpdate.getPassword());

        userValidationService.validateUniqueUserName(updateData.getUsername(), updateData.getId());
        userValidationService.validateUniqueEmail(updateData.getEmail(), userToUpdate.getId());
        User updatedUser = modelMapperService.toUser(updateData, userToUpdate);
        return userRepository.save(updatedUser);
    }

    @Override
    public void passwordUpdate(UpdatePasswordDto updateData, User loggedUser) {
        checkIfCurrentPasswordIsCorrect(loggedUser.getPassword(), updateData.getOldPassword());
        String newPassword = passwordEncoder.encode(updateData.getNewPassword());
        loggedUser.setPassword(newPassword);
        userRepository.save(loggedUser);
    }

    @Override
    public void addRemoveBeerToDrunk(Beer beer, User user) {
        if (user.hasDrunkBeer(beer.getId()))
            user.removeBeerFromDrunk(beer);
        else
            user.addBeerToDrunk(beer);

        user.setUpdateDate(LocalDate.now().toString());
        userRepository.save(user);
    }

    @Override
    public void sendRegistrationConfirmationEmail(UserToken token, String url) throws MessagingException {
        String fullUrl = url + PATH_FOR_REGISTRATION_CONFIRMATION + token.getTokenValue();
        EmailDtoHtml emailDto = generateEmailDtoHtml(token, fullUrl, SUBJECT_FOR_REGISTRATION_CONFIRMATION,
                TEMPLATE_FOR_REGISTRATION_CONFIRMATION);
        emailSenderService.sendHtmlEmailFromTemplate(emailDto);
    }

    @Override
    public void sendPasswordRecoveryMail(UserToken token, String url) throws MessagingException {
        String fullUrl = url + PATH_FOR_PASSWORD_RECOVERY_HANDLER + token.getTokenValue();
        EmailDtoHtml emailDto = generateEmailDtoHtml(token, fullUrl, SUBJECT_FOR_PASSWORD_RECOVERY, TEMPLATE_FOR_PASSWORD_RECOVERY);
        emailSenderService.sendHtmlEmailFromTemplate(emailDto);
    }

    @Override
    public void addRemoveBeerFromFavourite(Beer beer, User user) {
        if (user.hasFavouriteBeer(beer.getId()))
            user.removeBeerFromFavourite(beer);
        else
            user.addBeerToFavourite(beer);

        user.setUpdateDate(LocalDate.now().toString());
        userRepository.save(user);
    }

    @Override
    public void updateForgottenPassword(UpdateForgottenPasswordDto updateData) {
        User user = userServiceFinder.getById(updateData.getId());
        UserToken token = tokenService.searchByName(updateData.getValidationToken());

        userValidationService.validateToken(user, token);
        String newPassword = passwordEncoder.encode(updateData.getNewPassword());
        user.setPassword(newPassword);
        userRepository.save(user);
    }

    private EmailDtoHtml generateEmailDtoHtml(UserToken token, String url, String subject, String template) {
        User user = token.getUser();
        String username = user.getFirstName() + " " + user.getLastName();
        String userEmail = user.getEmail();

        Map<String, Object> htmlParameters = Map.of("username", username, "url", url);

        return new EmailDtoHtml(userEmail, subject, template, htmlParameters);
    }

    private void checkIfCurrentPasswordIsCorrect(String currentPassword, String passwordForValidation) {
        if (passwordEncoder.matches(passwordForValidation, currentPassword)) {
            throw new AuthorisationException(WRONG_CONFIRMATION_PASSWORD);
        }
    }
}
