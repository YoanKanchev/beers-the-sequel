package com.beer_tag.services;

import com.beer_tag.models.dto.beers.CreateBeerDto;
import com.beer_tag.models.impl.Beer;
import com.beer_tag.models.impl.Rating;
import com.beer_tag.models.impl.Tag;
import com.beer_tag.models.impl.User;

public interface BeerServiceActions {

		Beer create(CreateBeerDto beerDto, User loggedUser);

		void deactivate(int beerId, User loggedUser);

		void activate(int beerId, User loggedUser);

		Beer update(CreateBeerDto beer, User loggedUser);

		Beer tagBeer(Beer beer, Tag tag, User loggedUser);

		Beer rateBeer(Beer beer, Rating rating, User loggedUser);

		void removeRating(int ratingId, Beer beer, User loggedUser);
}
