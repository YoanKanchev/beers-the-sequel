package com.beer_tag.services;

import com.beer_tag.exceptions.AuthorisationException;
import com.beer_tag.exceptions.EntityNotFoundException;
import com.beer_tag.models.impl.Tag;
import com.beer_tag.models.impl.User;
import com.beer_tag.repositories.TagRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import static com.beer_tag.heplers.GlobalConstants.*;

@Service
public class TagServiceImpl implements TagService {

    private final TagRepository tagRepository;

    @Autowired
    public TagServiceImpl(TagRepository tagRepository) {
        this.tagRepository = tagRepository;
    }

    @Override
    public Tag create(Tag tag, User loggedUser) {
        Optional<Tag> presentTag = tagRepository.findByName(tag.getName());
        return presentTag.orElseGet(() -> tagRepository.save(tag));
    }

    @Override
    public void delete(Tag tag, User user) {
        userAuthorisationValidation(user);
        tagRepository.delete(tag);
    }

    @Override
    public Tag getById(int id) {
        return tagRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException(String.format(TAG_ENTITY + WITH_NAME + NOT_FOUND, id)));
    }

    @Override
    public Optional<Tag> getByName(String tagName) {
        return tagRepository.findByName(tagName);
    }

    @Override
    public List<Tag> getAll() {
        return tagRepository.findAll();
    }

    @Override
    public List<Tag> getAllFromCollectionOfNames(Set<String> names) {
        return tagRepository.findAllByNameIn(names);
    }

    private void userAuthorisationValidation(User loggedUser) {
        if (!loggedUser.isEnabled()
                && !loggedUser.hasRole(ROLE_ADMIN)) {
            throw new AuthorisationException(YOU_ARE_UNAUTHORISED);
        }
    }
}
