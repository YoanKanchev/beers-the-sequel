package com.beer_tag.services;

import com.beer_tag.models.impl.Style;
import com.beer_tag.models.impl.User;

import java.util.List;

public interface StyleService {

		List<Style> getAll();

		void create(Style style);

		Style update(Style style, User loggedUser);

		Style getById(int id);

		void delete(int id, User loggedUser);
}
