package com.beer_tag.services;

import com.beer_tag.models.dto.beers.BeerFilterDto;
import com.beer_tag.models.impl.Beer;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface BeerServiceFinder {

		Beer getById(int id);

		Beer getByName(String name);

		List<Beer> getAll();

		List<Beer> filter(BeerFilterDto beerFilterDto);

		Beer filterOne(BeerFilterDto beerFilterDto);

		Page<Beer> findPaginated(Pageable pageable, List<Beer> beerList);

		List<Beer> getSimilarBeers(Beer beer);
}
