package com.beer_tag.services;

import com.beer_tag.models.impl.User;
import com.beer_tag.models.impl.UserToken;

import java.util.List;

public interface TokenService {

		List<UserToken> getAll();

		UserToken searchByName(String tokenValue);

		UserToken searchById(int id);

		void delete(int id, User loggedUser);

		UserToken newToken(User user);
}
