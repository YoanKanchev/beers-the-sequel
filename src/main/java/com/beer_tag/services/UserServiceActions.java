package com.beer_tag.services;

import com.beer_tag.models.dto.UpdatePasswordDto;
import com.beer_tag.models.dto.users.CreateUserDto;
import com.beer_tag.models.dto.users.DeleteUserDto;
import com.beer_tag.models.dto.users.UpdateForgottenPasswordDto;
import com.beer_tag.models.dto.users.UpdateUserDto;
import com.beer_tag.models.impl.Beer;
import com.beer_tag.models.impl.User;
import com.beer_tag.models.impl.UserToken;

import javax.mail.MessagingException;

public interface UserServiceActions {

		User createUser(CreateUserDto user);

		User update(UpdateUserDto updateData, User loggedUser);

		void activate(int id, User loggedUser);

		void activateWithToken(User user, UserToken token);

		void deactivate(DeleteUserDto deleteUserDto, User loggedUser);

		void passwordUpdate(UpdatePasswordDto passwordDto, User loggedUser);

		void sendRegistrationConfirmationEmail(UserToken token, String url) throws MessagingException;

		void sendPasswordRecoveryMail(UserToken token, String url) throws MessagingException;

		void addRemoveBeerToDrunk(Beer beer, User user);

		void addRemoveBeerFromFavourite(Beer beer, User user);

		void updateForgottenPassword(UpdateForgottenPasswordDto updatePasswordDto);
}
