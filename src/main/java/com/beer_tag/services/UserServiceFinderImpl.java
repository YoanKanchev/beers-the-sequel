package com.beer_tag.services;

import com.beer_tag.exceptions.EntityNotFoundException;
import com.beer_tag.heplers.FilterBuilder;
import com.beer_tag.heplers.PaginationHelper;
import com.beer_tag.heplers.specifications.FilterSpecification;
import com.beer_tag.models.dto.users.UserFilterDto;
import com.beer_tag.models.impl.User;
import com.beer_tag.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

import static com.beer_tag.heplers.GlobalConstants.*;

@Service
public class UserServiceFinderImpl implements UserServiceFinder {

    private final UserRepository userRepository;
    private final FilterBuilder filterBuilder;
    private final PaginationHelper paginationHelper;

    @Autowired
    public UserServiceFinderImpl(UserRepository userRepository, FilterBuilder filterBuilder, PaginationHelper paginationHelper) {
        this.userRepository = userRepository;
        this.filterBuilder = filterBuilder;
        this.paginationHelper = paginationHelper;
    }

    public List<User> getAll() {
        return userRepository.findAll();
    }

    @Override
    public User getById(int id) {
        return userRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException(String.format(USER_ENTITY + WITH_ID + NOT_FOUND, id)));
    }

    @Override
    public User getByUsername(String username) {
        return userRepository.findByUsername(username)
                .orElseThrow(() -> new EntityNotFoundException(String.format(USER_ENTITY + WITH_NAME + NOT_FOUND, username)));
    }

    @Override
    public User getByEmail(String email) {
        return userRepository.findByEmail(email)
                .orElseThrow(() -> new EntityNotFoundException(String.format(USER_ENTITY + WITH_NAME + NOT_FOUND, email)));
    }

    @Override
    public Optional<User> findByUsername(String username) {
        return userRepository.findByUsername(username);
    }

    @Override
    public Optional<User> findByEmail(String email) {
        return userRepository.findByEmail(email);
    }

    @Override
    public List<User> filter(UserFilterDto userFilterDto) {
        FilterSpecification<User> userFilterSpecification = filterBuilder.createSpecification(userFilterDto);
        Sort sort = filterBuilder.getSortOrders(userFilterDto);
        return userRepository.findAll(userFilterSpecification, sort);
    }

    @Override
    public Page<User> findPaginated(Pageable pageable, List<User> allUsers) {
        return paginationHelper.findPaginated(pageable, allUsers);
    }

}
