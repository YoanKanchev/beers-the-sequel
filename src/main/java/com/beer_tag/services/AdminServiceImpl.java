package com.beer_tag.services;

import com.beer_tag.models.dto.users.UpdateUserFromAdminDto;
import com.beer_tag.models.enums.Role;
import com.beer_tag.models.impl.User;
import com.beer_tag.models.impl.UserRole;
import com.beer_tag.repositories.UserRepository;
import com.beer_tag.services.mapping.ModelMapperService;
import com.beer_tag.services.validation.BaseValidationsService;
import com.beer_tag.services.validation.UserValidationService;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Objects;
import java.util.Set;

import static com.beer_tag.heplers.GlobalConstants.DEFAULT_PICTURE;

@Service
public class AdminServiceImpl implements AdminService {

    private final UserRepository userRepository;
    private final UserServiceFinder userServiceFinder;
    private final ModelMapperService modelMapperService;
    private final UserValidationService userValidationService;

    @Autowired
    public AdminServiceImpl(UserRepository userRepository, UserServiceFinder userServiceFinder,
                            ModelMapperService modelMapperService, UserValidationService userValidationService) {
        this.userRepository = userRepository;
        this.userServiceFinder = userServiceFinder;
        this.modelMapperService = modelMapperService;
        this.userValidationService = userValidationService;

    }

    @Override
    public User updateUser(UpdateUserFromAdminDto updateData, User loggedUser) {
        userValidationService.validateAdmin(loggedUser);
        userValidationService.validateUniqueUserName(updateData.getUsername(), updateData.getId());
        User userToUpdate = userServiceFinder.getById(updateData.getId());
        userToUpdate = modelMapperService.toUser(updateData, userToUpdate);

        if (updateData.isPictureReset())
            userToUpdate.setPicture(DEFAULT_PICTURE);

        return userRepository.save(userToUpdate);
    }
}
