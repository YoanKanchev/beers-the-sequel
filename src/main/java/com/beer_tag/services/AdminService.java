package com.beer_tag.services;

import com.beer_tag.models.dto.users.UpdateUserFromAdminDto;
import com.beer_tag.models.impl.User;

public interface AdminService {

		User updateUser(UpdateUserFromAdminDto updateData, User loggedUser);
}
