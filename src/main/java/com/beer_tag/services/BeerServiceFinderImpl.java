package com.beer_tag.services;

import com.beer_tag.exceptions.EntityNotFoundException;
import com.beer_tag.heplers.FilterBuilder;
import com.beer_tag.heplers.PaginationHelper;
import com.beer_tag.heplers.specifications.FilterSpecification;
import com.beer_tag.models.dto.beers.BeerFilterDto;
import com.beer_tag.models.impl.Beer;
import com.beer_tag.repositories.BeerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static com.beer_tag.heplers.GlobalConstants.AVG_RATING;
import static com.beer_tag.heplers.GlobalConstants.BEER_ENTITY;
import static com.beer_tag.heplers.GlobalConstants.NAME;
import static com.beer_tag.heplers.GlobalConstants.NOT_FOUND;
import static com.beer_tag.heplers.GlobalConstants.WITH_ID;
import static com.beer_tag.heplers.GlobalConstants.WITH_NAME;

@Service
public class BeerServiceFinderImpl implements BeerServiceFinder {

		private final BeerRepository   beerRepository;
		private final TagService       tagService;
		private final FilterBuilder    filterBuilder;
		private final PaginationHelper paginationHelper;

		@Autowired
		public BeerServiceFinderImpl(BeerRepository beerRepository, TagService tagService, FilterBuilder filterBuilder,
				PaginationHelper paginationHelper) {
				this.beerRepository = beerRepository;
				this.tagService = tagService;
				this.filterBuilder = filterBuilder;
				this.paginationHelper = paginationHelper;
		}

		@Override
		public Beer getById(int id) {
				return beerRepository.findById(id)
						.orElseThrow(() -> new EntityNotFoundException(String.format(BEER_ENTITY + WITH_ID + NOT_FOUND, id)));
		}

		@Override
		public List<Beer> getAll() {
				return beerRepository.findAll();
		}

		@Override
		public Beer getByName(String name) {
				return beerRepository.findByName(name)
						.orElseThrow(() -> new EntityNotFoundException(String.format(BEER_ENTITY + WITH_NAME + NOT_FOUND, name)));
		}

		@Override
		public List<Beer> filter(BeerFilterDto beerFilterDto) {
				Set<String> tagNames = getTags(beerFilterDto);

				if (!tagNames.isEmpty()) {
						beerFilterDto.setName(null);
						beerFilterDto.setTags(tagService.getAllFromCollectionOfNames(tagNames));
				}
				FilterSpecification<Beer> beerFilterSpecification = filterBuilder.createSpecification(beerFilterDto);
				Sort sort = filterBuilder.getSortOrders(beerFilterDto);
				return beerRepository.findAll(beerFilterSpecification, sort);
		}

		@Override
		public Beer filterOne(BeerFilterDto beerFilterDto) {
				FilterSpecification<Beer> beerFilterSpecification = filterBuilder.createSpecification(beerFilterDto);
				return beerRepository.findOne(beerFilterSpecification)
						.orElseThrow(() -> new EntityNotFoundException(BEER_ENTITY + NOT_FOUND));
		}

		@Override
		public List<Beer> getSimilarBeers(Beer beer) {
				BeerFilterDto beerFilterDto = new BeerFilterDto();
				beerFilterDto.setStyleId(beer.getStyle().getId());
				beerFilterDto.setSortBy(NAME);
				FilterSpecification<Beer> beerFilterSpecification = filterBuilder.createSpecification(beerFilterDto);
				Sort sort = filterBuilder.getSortOrders(beerFilterDto);
				List<Beer> filtered = beerRepository.findAll(beerFilterSpecification, sort);
				filtered.remove(beer);
				return filtered;
		}

		@Override
		public Page<Beer> findPaginated(Pageable pageable, List<Beer> beerList) {
				return paginationHelper.findPaginated(pageable, beerList);
		}

		protected Set<String> getTags(BeerFilterDto filterDto) {
				String name = filterDto.getName();
				Set<String> tagNames = new HashSet<>();
				if (name != null && (!name.equals("")) && name.charAt(0) == '#') {
						tagNames = Arrays.stream(name.replaceAll("#", "").split(", ")).collect(Collectors.toSet());
				}
				return tagNames;
		}
}
