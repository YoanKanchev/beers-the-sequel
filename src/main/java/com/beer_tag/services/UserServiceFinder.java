package com.beer_tag.services;

import com.beer_tag.models.dto.users.UserFilterDto;
import com.beer_tag.models.impl.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

public interface UserServiceFinder {

		User getById(int id);

		User getByEmail(String email);

		User getByUsername(String username);

		Optional<User> findByUsername(String username);

		Optional<User> findByEmail(String email);

		List<User> getAll();

		List<User> filter(UserFilterDto userFilterDto);

		Page<User> findPaginated(Pageable pageable,List<User> allUsers);
}
