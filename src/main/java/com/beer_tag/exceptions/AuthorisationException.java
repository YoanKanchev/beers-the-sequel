package com.beer_tag.exceptions;

public class AuthorisationException extends RuntimeException {

    public AuthorisationException(String message) {
        super(message);
    }
}
