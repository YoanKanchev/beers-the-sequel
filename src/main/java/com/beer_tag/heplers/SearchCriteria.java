package com.beer_tag.heplers;

import com.beer_tag.models.impl.Tag;
import lombok.Data;

import java.util.Collection;

@Data
public class SearchCriteria {

    private Object             value;
    private Collection<Tag> values;
    private Object             searchBy;

    public SearchCriteria(Object value, Object operation) {
        this.value = value;
        this.searchBy = operation;
    }

    public SearchCriteria(Collection<Tag> values, Object operation) {
        this.values = values;
        this.searchBy = operation;
    }

    public Object getValue() {
        return value;
    }

    public Object getSearchBy() {
        return searchBy;
    }
}
