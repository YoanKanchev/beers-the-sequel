package com.beer_tag.heplers;

import com.beer_tag.heplers.specifications.FilterSpecification;
import com.beer_tag.heplers.specifications.impl.BeerFilterSpecificationImpl;
import com.beer_tag.heplers.specifications.impl.UserFilterSpecificationImpl;
import com.beer_tag.models.dto.FilterDto;
import com.beer_tag.models.dto.beers.BeerFilterDto;
import com.beer_tag.models.dto.users.UserFilterDto;
import com.beer_tag.models.impl.Beer;
import com.beer_tag.models.impl.Tag;
import com.beer_tag.models.impl.User;
import org.apache.logging.log4j.util.Strings;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import static com.beer_tag.heplers.specifications.operations.BeerSearchOperation.ID;
import static com.beer_tag.heplers.specifications.operations.BeerSearchOperation.*;
import static com.beer_tag.heplers.specifications.operations.UserSearchOperation.ACTIVE;
import static com.beer_tag.heplers.specifications.operations.UserSearchOperation.*;

@Component
public class FilterBuilder {

		public FilterSpecification<User> createSpecification(UserFilterDto userFilterDto) {
				return buildUserSpecification(userFilterDto.getUsernameOrMail(), userFilterDto.getIsActive(),
						userFilterDto.getRole());
		}

		public FilterSpecification<Beer> createSpecification(BeerFilterDto beerFilter) {
				return buildBeerSpecification(beerFilter);
		}

		public Sort getSortOrders(FilterDto filterParameter) {
				boolean isOrderAscending = filterParameter.isOrderAscending();
				if (filterParameter.isSorted()) {
						return Sort.by(getSortDirection(isOrderAscending), filterParameter.getSortBy());
				} else
						return isOrderAscending ? Sort.unsorted().ascending() : Sort.unsorted().descending();
		}

		public Sort.Direction getSortDirection(Boolean isOrderAsc) {
				return isOrderAsc ? Sort.Direction.ASC : Sort.Direction.DESC;
		}

		private FilterSpecification<User> buildUserSpecification(String usernameOrMail, String isActive, String role) {
				FilterSpecification<User> userFilterSpecification = new UserFilterSpecificationImpl();

				if (Strings.isNotEmpty(usernameOrMail))
						userFilterSpecification.add(new SearchCriteria(usernameOrMail, USERNAMEOREMAIL));

				if (Strings.isNotEmpty(isActive))
						userFilterSpecification.add(new SearchCriteria(Boolean.valueOf(isActive), ACTIVE));
				if (Strings.isNotEmpty(role))
						userFilterSpecification.add(new SearchCriteria(role, ROLE));

				return userFilterSpecification;
		}

		private FilterSpecification<Beer> buildBeerSpecification(BeerFilterDto beerFilter) {
				FilterSpecification<Beer> beerFilterSpecification = new BeerFilterSpecificationImpl();
				Integer id = beerFilter.getId();
				String name = beerFilter.getName();
				Collection<Tag> tags = beerFilter.getTags();
				Integer countryId = beerFilter.getCountryId();
				Integer styleId = beerFilter.getStyleId();
				Integer breweryId = beerFilter.getBreweryId();
				Boolean isActive = beerFilter.getIsActive();

				if (!Objects.isNull(id))
						beerFilterSpecification.add(new SearchCriteria(id, ID));

				if (Strings.isNotEmpty(name))
						beerFilterSpecification.add(new SearchCriteria(name, NAME));

				if (!Objects.isNull(tags) && !tags.isEmpty())
						beerFilterSpecification.add(new SearchCriteria(tags, TAG));

				if (!Objects.isNull(countryId))
						beerFilterSpecification.add(new SearchCriteria(countryId, COUNTRY));

				if (!Objects.isNull(styleId))
						beerFilterSpecification.add(new SearchCriteria(styleId, STYLE));

				if (!Objects.isNull(breweryId))
						beerFilterSpecification.add(new SearchCriteria(breweryId, BREWERY));

				if (isActive != null)
						beerFilterSpecification.add(new SearchCriteria(isActive, ACTIVE));

				return beerFilterSpecification;
		}
}
