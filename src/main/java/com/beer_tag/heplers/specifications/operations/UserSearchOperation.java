package com.beer_tag.heplers.specifications.operations;

public enum UserSearchOperation {
		ID,
		USERNAMEOREMAIL,
		ACTIVE,
		ROLE,
}
