package com.beer_tag.heplers.specifications.operations;

public enum BeerSearchOperation {
		ID,
		NAME,
		TAG,
		COUNTRY,
		STYLE,
		BREWERY,
		ACTIVE
}
