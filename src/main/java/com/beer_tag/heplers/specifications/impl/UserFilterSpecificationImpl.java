package com.beer_tag.heplers.specifications.impl;

import com.beer_tag.heplers.SearchCriteria;
import com.beer_tag.heplers.specifications.FilterSpecification;
import com.beer_tag.heplers.specifications.operations.UserSearchOperation;
import com.beer_tag.models.impl.User;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

import static com.beer_tag.heplers.GlobalConstants.*;

public class UserFilterSpecificationImpl implements FilterSpecification<User> {

    private static final long serialVersionUID = 1900581010229669687L;
    private final List<SearchCriteria> list;

    public UserFilterSpecificationImpl() {
        this.list = new ArrayList<>();
    }

    @Override
    public void add(SearchCriteria criteria) {
        list.add(criteria);
    }

    @Override
    public Predicate toPredicate(Root<User> root, CriteriaQuery<?> query, CriteriaBuilder builder) {
        // create a new predicate list
        List<Predicate> predicates = new ArrayList<>();
        // add add criteria to predicates
        for (SearchCriteria criteria : list) {
            UserSearchOperation searchBy = UserSearchOperation.valueOf(criteria.getSearchBy().toString());
            Object value = criteria.getValue();
            switch (searchBy) {
                case ID: {
                    predicates.add(getPredicatesForId(root, builder, value));
                    break;
                }
                case USERNAMEOREMAIL: {
                    predicates.add(getPredicatesForUsernameOrMail(root, builder, value));
                }
                break;
                case ACTIVE: {
                    predicates.add(getPredicatesForActiveUsers(root, builder, value));
                    break;
                }
                case ROLE:
                    predicates.add(getPredicatesForRole(root, builder, value));
                    break;
            }
        }
        return builder.and(predicates.toArray(new Predicate[0]));
    }

    private Predicate getPredicatesForId(Root<User> root, CriteriaBuilder builder, Object value) {
        return builder.equal(root.get(ID), value.toString());
    }

    private Predicate getPredicatesForUsernameOrMail(Root<User> root, CriteriaBuilder builder, Object value) {
        Predicate username = builder.like(root
                .get(USERNAME), "%" + value + "%");
        Predicate email = builder.like(root.get(EMAIL), "%" + value.toString() + "%");
        return builder.or(username, email);
    }

    private Predicate getPredicatesForActiveUsers(Root<User> root, CriteriaBuilder builder, Object value) {
        return builder.equal(root.get(ENABLED), value);
    }

    private Predicate getPredicatesForRole(Root<User> root, CriteriaBuilder builder, Object value) {
        return builder.equal(root.join(AUTHORITIES).get(AUTHORITY), value);
    }
}
