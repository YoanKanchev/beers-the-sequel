package com.beer_tag.heplers.specifications.impl;

import com.beer_tag.heplers.SearchCriteria;
import com.beer_tag.heplers.specifications.FilterSpecification;
import com.beer_tag.heplers.specifications.operations.BeerSearchOperation;
import com.beer_tag.models.impl.Beer;
import com.beer_tag.models.impl.Tag;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static com.beer_tag.heplers.GlobalConstants.*;

public class BeerFilterSpecificationImpl implements FilterSpecification<Beer> {

	private static final long serialVersionUID = 1900581010229669687L;
    private final List<SearchCriteria> list;

    public BeerFilterSpecificationImpl() {
        this.list = new ArrayList<>();
    }

    @Override
    public void add(SearchCriteria criteria) {
        list.add(criteria);
    }

    @Override
    public Predicate toPredicate(Root<Beer> root, CriteriaQuery<?> query, CriteriaBuilder builder) {
        // create a new predicate list
        List<Predicate> predicates = new ArrayList<>();
        // add add criteria to predicates
        for (SearchCriteria criteria : list) {
            BeerSearchOperation searchBy = BeerSearchOperation.valueOf(criteria.getSearchBy().toString());
            Object value = criteria.getValue();
            switch (searchBy) {
                case ID:
                    getPredicatesForId(root, builder, predicates, value);
                    break;
                case NAME:
                    getPredicatesForName(root, builder, predicates, value);
                    break;

                case COUNTRY:
                    getPredicatesForCountry(root, builder, predicates, value);
                    break;

                case STYLE:
                    getPredicatesForStyle(root, builder, predicates, value);
                    break;
                case TAG:
                    getPredicatesForTags(root, builder, predicates, criteria);
                    break;

                case BREWERY:
                    getPredicatesForBrewery(root, builder, predicates, value);
                    break;

                case ACTIVE:
                    getPredicatesForIsActive(root, builder, predicates, value);
                    break;
            }
        }
        return builder.and(predicates.toArray(new Predicate[0]));
    }

    private void getPredicatesForIsActive(Root<Beer> root, CriteriaBuilder builder, List<Predicate> predicates, Object value) {
        predicates.add(builder.equal(root.get(ACTIVE), value));
    }

    private void getPredicatesForTags(Root<Beer> root, CriteriaBuilder builder, List<Predicate> predicates, SearchCriteria criteria) {
        Collection<Tag> tagIds = criteria.getValues();
        tagIds.forEach(tagId -> predicates.add(builder.isMember(tagId, root.get(TAGS))));
    }

    private void getPredicatesForStyle(Root<Beer> root, CriteriaBuilder builder, List<Predicate> predicates, Object value) {
        predicates.add(builder.equal(root.get(STYLE), value));
    }

    private void getPredicatesForName(Root<Beer> root, CriteriaBuilder builder, List<Predicate> predicates, Object value) {
        predicates.add(builder.like(root.get(NAME), "%" + value.toString() + "%"));
    }

    private void getPredicatesForId(Root<Beer> root, CriteriaBuilder builder, List<Predicate> predicates, Object value) {
        predicates.add(builder.equal(root.get(ID), value));
    }

    private void getPredicatesForCountry(Root<Beer> root, CriteriaBuilder builder, List<Predicate> predicates, Object value) {
        predicates.add(builder.equal(root.get(BREWERY).get(COUNTRY), value));
    }

    private void getPredicatesForBrewery(Root<Beer> root, CriteriaBuilder builder, List<Predicate> predicates, Object value) {
        predicates.add(builder.equal(root.get(BREWERY), value));
    }
}
