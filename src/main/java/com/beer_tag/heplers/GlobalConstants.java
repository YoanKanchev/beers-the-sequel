package com.beer_tag.heplers;

public class GlobalConstants {


		//LINK
		public static final String DEFAULT_PICTURE = "https://www.bsmc.net.au/wp-content/uploads/No-image-available.jpg";

		//URL PATHS
		public static final String PATH_FOR_PASSWORD_RECOVERY_HANDLER = "/sign-up/recover-password/verify?token=";
		public static final String PATH_FOR_REGISTRATION_CONFIRMATION = "/sign-up/verify?token=";

		//HTML PAGES
		public static final String TEMPLATE_FOR_PASSWORD_RECOVERY         = "password-reset-email-template.html";
		public static final String TEMPLATE_FOR_REGISTRATION_CONFIRMATION = "registration-confirmation-email-template.html";

		//EMAIL
		public static final String SUBJECT_FOR_REGISTRATION_CONFIRMATION = "Beers New Account Confirmation";
		public static final String SUBJECT_FOR_PASSWORD_RECOVERY         = "Beers Password Recovery";
		public static final String CONFIRMATION_EMAIL_SEND               = "An confirmation e-mail has been send to your e-mail address, please follow the link before continuing.";
		public static final String EMAIL_SEND                            = "An e-mail has been send to your e-mail address!";
		public static final String YOUR_LINK_HAS_EXPIRED                 = "Your link has expired...";

		//ENTITIES
		public static final String USER_ENTITY    = "User";
		public static final String BEER_ENTITY    = "Beer";
		public static final String TAG_ENTITY     = "Tag";
		public static final String TOKEN_ENTITY   = "Token";
		public static final String COUNTRY_ENTITY = "Country";
		public static final String STYLE_ENTITY   = "Style";
		public static final String RATING_ENTITY  = "Rating";

		//MESSAGES
		public static final String NOT_FOUND                   = " not found!";
		public static final String SUCCESSFULLY_CREATED        = " successfully created!";
		public static final String SUCCESSFULLY_UPDATED        = " successfully updated!";
		public static final String SUCCESSFULLY_DELETED        = " successfully deleted!";
		public static final String SUCCESSFULLY_ACTIVATE       = " successfully activated!";
		public static final String SUCCESSFULLY_RATED          = " successfully rated!";
		public static final String ALREADY_EXISTS              = " already exists!";
		public static final String WITH_ID                     = " with id:%d ";
		public static final String WITH_NAME                   = " with name:%s ";
		public static final String WITH_EMAIL                  = " with email:%s ";
		public static final String YOU_ARE_UNAUTHORISED        = "You are unauthorised!!";
		public static final String WRONG_CONFIRMATION_PASSWORD = "Wrong confirmation password!";

		//ACTIONS
		public static final String DELETE = "delete";
		public static final String UPDATE = "update";

		//SORT BY
		public static final String NAME       = "name";
		public static final String AVG_RATING = "avgRating";
		public static final String BREWERY    = "brewery";

		//USER ROLES
		public static final String ROLE_USER    = "ROLE_USER";
		public static final String ROLE_ADMIN   = "ROLE_ADMIN";
		public static final String ROLE_AGENT   = "ROLE_AGENT";
		public static final String INVALID_ROLE = "Invalid Role";

		//MVC MODEL ATTRIBUTE NAMES
		public static final String LOGGED_USER         = "loggedUser";
		public static final String PAGE_NUMBER_USERS   = "pageNumberUsers";
		public static final String PAGE_NUMBER_BEERS   = "pageNumberBeers";
		public static final String CURRENT_PAGE_USERS  = "currentPageUsers";
		public static final String USER_PAGE           = "userPage";
		public static final String BEER_PAGE           = "beerPage";
		public static final String ALL_BEERS           = "allBeers";
		public static final String ALL_USERS           = "allUsers";
		public static final String TAGS                = "tags";
		public static final String STYLES              = "styles";
		public static final String COUNTRIES           = "countries";
		public static final String USER_FILTER_DTO     = "userFilterDto";
		public static final String BEER_FILTER_DTO     = "beerFilterDto";
		public static final String UPDATE_USER_DTO     = "updateUserDto";
		public static final String UPDATE_BEER_DTO     = "updateBeerDto";
		public static final String CREATE_USER_DTO     = "createUserDto";
		public static final String CREATE_BEER_DTO     = "createBeerDto";
		public static final String BEER_FILTER_SESSION = "beerFilterSession";
		public static final String CURRENT_PAGE_BEERS  = "currentPageBeers";
		public static final String USER_FILTER_SESSION = "userFilterSession";
		public static final String BEER                = "beer";
		public static final String USER                = "user";
		public static final String BEERS               = "beers";
		public static final String SIMILAR_BEERS       = "similarBeers";
		public static final String RATING              = "rating";
		public static final String CREATE_TAG          = "createTag";

		//Entity fields (connected with database)
		public static final String ID                                  = "id";
		public static final String STYLE                               = "style";
		public static final String COUNTRY                             = "country";
		public static final String ACTIVE                              = "active";
		public static final String USERNAME                            = "username";
		public static final String ENABLED                             = "enabled";
		public static final String EMAIL                               = "email";
		public static final String AUTHORITIES                         = "authorities";
		public static final String AUTHORITY                           = "authority";
		public static final int    START_PAGE_PAGINATION               = 1;
		public static final int    NUMBER_OF_ITEMS_PER_PAGE_PAGINATION = 5;

		//REDIRECT ATTRIBUTES
		public static final String REDIRECT      = "redirect:";
		public static final String REDIRECT_HOME = "redirect:/";
		public static final String SUCCESS       = "success";
		public static final String ERROR         = "error";

		//ENCODING
		public static final String UTF_8                   = "UTF-8";
		public static final String SEARCH_BEER_DTO         = "searchBeerDto";
}
