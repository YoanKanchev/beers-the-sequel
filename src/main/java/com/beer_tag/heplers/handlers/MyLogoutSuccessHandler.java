package com.beer_tag.heplers.handlers;

import org.springframework.security.core.Authentication;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static org.springframework.http.HttpHeaders.REFERER;

public class MyLogoutSuccessHandler implements LogoutSuccessHandler {

    private final RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();

    @Override
    public void onLogoutSuccess(HttpServletRequest request,
                                HttpServletResponse response, Authentication authentication)
            throws IOException {

        handle(request, response);
    }

    protected void handle(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String targetUrl = request.getHeader(REFERER);
        redirectStrategy.sendRedirect(request, response, targetUrl);
    }
}
