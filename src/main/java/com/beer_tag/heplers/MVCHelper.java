package com.beer_tag.heplers;

import org.springframework.stereotype.Component;
import org.springframework.validation.BindingResult;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.StringJoiner;

import static com.beer_tag.heplers.GlobalConstants.*;

@Component
public class MVCHelper {

    public String redirectError(RedirectAttributes redirectAttributes, BindingResult bindingResult, String redirectUrl) {
        redirectAttributes.addFlashAttribute(ERROR, getErrors(bindingResult));
        return REDIRECT + redirectUrl;
    }

    public String redirectError(RedirectAttributes redirectAttributes, String errorMessage, String redirectUrl) {
        redirectAttributes.addFlashAttribute(ERROR, errorMessage);
        return REDIRECT + redirectUrl;
    }

    public String redirectError(RedirectAttributes redirectAttributes, Exception e, String redirectUrl) {
        redirectAttributes.addFlashAttribute(ERROR, e.getMessage());
        return REDIRECT + redirectUrl;
    }

    public String redirectSuccess(RedirectAttributes redirectAttributes, String message, String redirectUrl) {
        redirectAttributes.addFlashAttribute(SUCCESS, message);
        return REDIRECT + redirectUrl;
    }

    private String getErrors(BindingResult bindingResult) {
        StringJoiner output = new StringJoiner(", \n");
        bindingResult.getAllErrors()
                .forEach(objectError -> output.add(objectError.getDefaultMessage()));
        return output.toString();
    }
}
