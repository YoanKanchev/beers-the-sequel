package com.beer_tag.repositories;

import com.beer_tag.models.impl.Tag;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.Set;

@Repository
public interface TagRepository extends JpaRepository<Tag, Integer> {

		Optional<Tag> findByName(String tagName);

		List<Tag> findAllByNameIn(Set<String> name);
}
