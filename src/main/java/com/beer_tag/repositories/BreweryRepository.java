package com.beer_tag.repositories;

import com.beer_tag.models.impl.Brewery;
import com.beer_tag.models.impl.Country;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface BreweryRepository extends JpaRepository<Brewery, Integer> {

		Optional<Brewery> findByName(String name);

		List<Brewery> findAllByCountry(Country country);
}
