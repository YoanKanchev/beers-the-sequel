package com.beer_tag.repositories;

import com.beer_tag.models.impl.Rating;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RatingRepository extends JpaRepository<Rating, Integer> {

}
