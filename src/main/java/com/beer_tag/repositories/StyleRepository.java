package com.beer_tag.repositories;

import com.beer_tag.models.impl.Style;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface StyleRepository extends JpaRepository<Style, Integer> {

		Optional<Style> findByName(String name);

}
