package com.beer_tag.repositories;

import com.beer_tag.models.impl.Beer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface BeerRepository extends JpaRepository<Beer, Integer>, JpaSpecificationExecutor<Beer> {

		Optional<Beer> findByName(String name);

		Optional<Beer> findByIdAndActive(int id, boolean isActive);

		List<Beer> findAllByActive(boolean isActive);

		Beer findByNameAndActive(String name, Boolean isActive);
}
