package com.beer_tag.repositories;

import com.beer_tag.models.impl.UserToken;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface TokenRepository extends JpaRepository<UserToken, Integer> {

		Optional<UserToken> findByTokenValue(String tokenValue);
}
