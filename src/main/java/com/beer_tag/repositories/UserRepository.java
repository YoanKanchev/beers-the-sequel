package com.beer_tag.repositories;

import com.beer_tag.models.impl.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Integer>
		, JpaSpecificationExecutor<User> {

		boolean existsByUsername(String name);

		boolean existsByEmail(String name);

		Optional<User> findByEmail(String email);

		Optional<User> findByUsername(String username);

		List<User> findAllByEnabled(boolean isEnabled);

}
