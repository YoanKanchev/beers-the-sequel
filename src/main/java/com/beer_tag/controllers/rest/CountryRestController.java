package com.beer_tag.controllers.rest;

import com.beer_tag.models.impl.Brewery;
import com.beer_tag.models.impl.Country;
import com.beer_tag.services.BreweryService;
import com.beer_tag.services.CountryService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/countries")
public class CountryRestController {

		private final CountryService countryService;
		private final BreweryService breweryService;

		public CountryRestController(CountryService countryService, BreweryService breweryService) {
				this.countryService = countryService;
				this.breweryService = breweryService;
		}

		@GetMapping
		public List<Country> getAll() {
				return countryService.getAll();
		}

		@GetMapping("/{id}/breweries")
		public List<Brewery> getBreweriesByCountryId(@PathVariable int id) {
				return breweryService.findAllByCountry(getById(id));
		}

		@GetMapping("/{id}")
		public Country getById(@PathVariable int id) {
				return countryService.getById(id);
		}

}
