package com.beer_tag.controllers.rest;

import com.beer_tag.exceptions.DuplicateEntityException;
import com.beer_tag.models.dto.beers.CreateBreweryDto;
import com.beer_tag.models.impl.Brewery;
import com.beer_tag.models.impl.User;
import com.beer_tag.services.BreweryService;
import com.beer_tag.services.UserServiceFinder;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("/api/breweries")
public class BreweryRestController {

		private final BreweryService    breweryService;
		private final UserServiceFinder userServiceFinder;

		public BreweryRestController(BreweryService breweryService, UserServiceFinder userServiceFinder) {
				this.breweryService = breweryService;
				this.userServiceFinder = userServiceFinder;
		}

		@GetMapping
		public List<Brewery> getAll() {
				return breweryService.getAll();
		}

		@PostMapping
		public void create(@ModelAttribute CreateBreweryDto createData) {
				try {
						breweryService.create(createData);
				} catch (DuplicateEntityException e) {
						throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
				}
		}

		@PutMapping
		public Brewery update(CreateBreweryDto updateData, Principal principal) {
				User loggedUser = userServiceFinder.getByUsername(principal.getName());
				return breweryService.update(updateData, loggedUser);
		}

		@GetMapping("/{id}")
		public Brewery getById(@PathVariable int id) {
				return breweryService.getById(id);
		}

		@DeleteMapping
		public void delete(@RequestParam int id, Principal principal) {
				User loggedUser = userServiceFinder.getByUsername(principal.getName());
				breweryService.delete(id, loggedUser);
		}
}
