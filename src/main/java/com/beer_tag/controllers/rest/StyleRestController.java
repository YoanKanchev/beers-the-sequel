package com.beer_tag.controllers.rest;

import com.beer_tag.models.impl.Style;
import com.beer_tag.models.impl.User;
import com.beer_tag.services.StyleService;
import com.beer_tag.services.UserServiceFinder;
import com.beer_tag.services.mapping.ModelMapperService;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("/api/styles")
public class StyleRestController {

		private final StyleService       service;
		private final ModelMapperService mapperService;
		private final UserServiceFinder  userServiceFinder;

		public StyleRestController(StyleService service, ModelMapperService mapperService,
				UserServiceFinder userServiceFinder) {
				this.service = service;
				this.mapperService = mapperService;
				this.userServiceFinder = userServiceFinder;
		}

		@GetMapping
		public List<Style> getAll() {
				return service.getAll();
		}

		@PostMapping
		public void create(Style style) {
				service.create(style);
		}

		@PutMapping
		public Style update(Style style, Principal principal) {
				User loggedUser = userServiceFinder.getByUsername(principal.getName());
				Style currentStyle = service.getById(style.getId());
				mapperService.toStyle(style, currentStyle);
				return service.update(style, loggedUser);
		}

		@GetMapping("{/id}")
		public Style searchById(@PathVariable int id) {
				return service.getById(id);
		}

		@DeleteMapping
		public void delete(@RequestParam int id, Principal principal) {
				User loggedUser = userServiceFinder.getByUsername(principal.getName());
				service.delete(id, loggedUser);

		}
}
