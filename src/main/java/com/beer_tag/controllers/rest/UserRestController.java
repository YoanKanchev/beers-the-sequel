package com.beer_tag.controllers.rest;

import com.beer_tag.exceptions.EntityNotFoundException;
import com.beer_tag.models.dto.users.UserFilterDto;
import com.beer_tag.models.impl.User;
import com.beer_tag.services.UserServiceFinder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("/api/users")
public class UserRestController {

		private final UserServiceFinder userServiceFinder;

		@Autowired
		public UserRestController(UserServiceFinder userServiceFinder) {
				this.userServiceFinder = userServiceFinder;
		}

		@GetMapping()
		public List<User> getAllUsers() {
				return userServiceFinder.getAll();
		}

		@GetMapping("/{id}")
		public User getById(@PathVariable int id) {
				try {
						return userServiceFinder.getById(id);

				} catch (EntityNotFoundException e) {
						throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
				}
		}

		@GetMapping("/existUsername")
		public boolean existUsername(@RequestParam String username) {
				return userServiceFinder.findByUsername(username).isPresent();
		}

		@GetMapping("/existEmail")
		public boolean existEmail(@RequestParam String email) {
				return userServiceFinder.findByEmail(email).isPresent();
		}

		@GetMapping("/")
		public List<User> filter(@ModelAttribute UserFilterDto searchData) {
				return userServiceFinder.filter(searchData);
		}

		//		@GetMapping("/drunk")
		//		public List<Beer> getDrunkBeers(Principal principal) {
		//				User user = service.getByUsername(principal.getName());
		//				return new ArrayList<>(user.getDrunkBeers());
		//		}
		//
		//		@GetMapping("/favourite")
		//		public List<Beer> getFavouriteBeers(Principal principal) {
		//				User user = service.getByUsername(principal.getName());
		//				return new ArrayList<>(user.getFavouriteBeers());
		//		}
		//
		//		@PostMapping("/")
		//		public User createUser(@Valid @RequestBody CreateUserDto updateUserDto) {
		//				try {
		//						User user = userInputUserCreate(updateUserDto);
		//
		//						//						service.createUser(user);
		//
		//						return user;
		//
		//				} catch (DuplicateEntityException e) {
		//						throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
		//				}
		//		}
		//
		//		@PostMapping("/drunk/")
		//		public void addBeerToDrunk(Principal principal, @RequestParam int beerID) {
		//				try {
		//
		//						User user = service.getByUsername(principal.getName());
		//						Beer beer = beerService.getById(beerID);
		//						if (user.hasFavBeer(beer))
		//								service.addRemoveBeerFromFavourite(beer, user);
		//						else
		//								service.addBeerToFavourite(beer, user);
		//
		//						service.addRemoveBeerToDrunk(beer, user);
		//
		//				} catch (EntityNotFoundException eE) {
		//						throw new ResponseStatusException(HttpStatus.NOT_FOUND, eE.getMessage());
		//				} catch (AuthorisationException e) {
		//						throw new ResponseStatusException(HttpStatus.FORBIDDEN, e.getMessage());
		//				}
		//
		//		}
		//
		//		@PostMapping("/favourite")
		//		public void addBeerToFavourite(Principal principal, @RequestParam int beerID) {
		//				Beer beer = beerService.getById(beerID);
		//				User user = service.getByUsername(principal.getName());
		//
		//				service.addBeerToFavourite(beer, user);
		//		}
		//
		//		@PutMapping("/")
		//		public User update(@Valid @RequestBody CreateUserDto updateUserDto, Principal principal) {
		//				try {
		//
		//						User currentUser = service.getByUsername(principal.getName());
		//						User userUpdate = RestUserUpdater(updateUserDto, currentUser);
		//						service.update(userUpdate);
		//
		//						service.update(userUpdate);
		//
		//						return userUpdate;
		//
		//				} catch (EntityNotFoundException e) {
		//						throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
		//				} catch (DuplicateEntityException e) {
		//						throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
		//				}
		//		}
		//
		//		@DeleteMapping("/")
		//		public void delete(Principal principal) {
		//				try {
		//						LocalDateTime time = LocalDateTime.now();
		//						User user = service.getByUsername(principal.getName());
		//						user.setActive(false);
		//						user.setDeactivationDate(String.valueOf(time));
		//						user.setUpdateDate(String.valueOf(time));
		//						service.deactivate(user);
		//				} catch (EntityNotFoundException e) {
		//						throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
		//				}
		//		}
		//
		//		@DeleteMapping("/drunk")
		//		public void removeBeerFromDrunk(Principal principal, @RequestParam int beerID) {
		//				Beer beer = beerService.getById(beerID);
		//				User user = service.getByUsername(principal.getName());
		//
		//				service.addRemoveBeerFromDrunk(beer, user);
		//		}
		//
		//		@DeleteMapping("/favourite")
		//		public void removeBeerFromFavourite(Principal principal, @RequestParam int beerID) {
		//				Beer beer = beerService.getById(beerID);
		//				User user = service.getByUsername(principal.getName());
		//
		//				service.addRemoveBeerFromFavourite(beer, user);
		//		}
}