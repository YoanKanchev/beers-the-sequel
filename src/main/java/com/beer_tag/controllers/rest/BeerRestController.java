package com.beer_tag.controllers.rest;

import com.beer_tag.models.dto.beers.CreateBeerDto;
import com.beer_tag.models.impl.Beer;
import com.beer_tag.models.impl.Rating;
import com.beer_tag.models.impl.Tag;
import com.beer_tag.models.impl.User;
import com.beer_tag.services.BeerServiceActions;
import com.beer_tag.services.BeerServiceFinder;
import com.beer_tag.services.UserServiceFinder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/beers")
public class BeerRestController {

		private final BeerServiceFinder  beerServiceFinder;
		private final BeerServiceActions beerServiceActions;
		private final UserServiceFinder  userServiceFinder;

		public BeerRestController(BeerServiceFinder beerServiceFinder, BeerServiceActions beerServiceActions,
				UserServiceFinder userServiceFinder) {
				this.beerServiceFinder = beerServiceFinder;
				this.beerServiceActions = beerServiceActions;
				this.userServiceFinder = userServiceFinder;
		}

		@GetMapping()
		public List<Beer> getAll() {
				return beerServiceFinder.getAll();
		}

		@GetMapping("/{id}")
		public Beer getById(@PathVariable int id) {
				return beerServiceFinder.getById(id);
		}

		//		@GetMapping("/")
		//		public List<Beer> filter(@RequestParam(required = false) String name,
		//				@RequestParam(required = false) Integer countryId,
		//				@RequestParam(required = false) Integer styleId,
		//				@RequestParam(required = false) String sortBy,
		//				@RequestParam(required = false) Integer limitResult) {
		//
		//				return beerService.filter(name, countryId, styleId, sortBy, limitResult);
		//		}

		@GetMapping("/{id}/tag")
		public List<Tag> getTags(@PathVariable int id) {
				Beer beer = beerServiceFinder.getById(id);
				return new ArrayList<>(beer.getTags());
		}

		@PostMapping
		public Beer create(@RequestBody CreateBeerDto beer, Principal principal) {
				User loggedUser = userServiceFinder.getByUsername(principal.getName());
				return beerServiceActions.create(beer, loggedUser);
		}

		@PostMapping("/{beerId}/tag")
		public Beer tagBeer(@PathVariable int beerId, @RequestParam String tagName, Principal principal) {
				Beer beer = beerServiceFinder.getById(beerId);
				User loggedUser = userServiceFinder.getByUsername(principal.getName());
				Tag tag = new Tag();
				tag.setName(tagName);
				return beerServiceActions.tagBeer(beer, tag, loggedUser);
		}

		@PutMapping
		public Beer update(@Valid @RequestBody CreateBeerDto beerDto, Principal principal) {
				User loggedUser = userServiceFinder.getByUsername(principal.getName());
				return beerServiceActions.update(beerDto, loggedUser);
		}

		@PutMapping("/{beerId}/rate")
		public Beer rateBeer(@PathVariable int beerId, @RequestBody Rating rating, Principal principal) {
				Beer beer = beerServiceFinder.getById(beerId);
				User loggedUser = userServiceFinder.getByUsername(principal.getName());
				rating.setBeer(beer);
				rating.setCreatedBy(loggedUser);
				return beerServiceActions.rateBeer(beer, rating, loggedUser);
		}

		@DeleteMapping
		public void delete(@RequestParam int id, Principal principal) {
				User loggedUser = userServiceFinder.getByUsername(principal.getName());
				beerServiceActions.deactivate(id, loggedUser);
		}
}
