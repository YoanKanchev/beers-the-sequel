package com.beer_tag.controllers.mvc;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.SessionAttributes;

import javax.servlet.http.HttpServletRequest;

@Controller
public class LogInController {

		@GetMapping("/logIn")
		public String showLogInPage() {
				return "logIn-page";
		}
}
