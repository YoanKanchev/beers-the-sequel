package com.beer_tag.controllers.mvc;

import com.beer_tag.exceptions.EntityNotFoundException;
import com.beer_tag.heplers.MVCHelper;
import com.beer_tag.models.dto.beers.BeerFilterDto;
import com.beer_tag.models.dto.beers.CreateBeerDto;
import com.beer_tag.models.dto.beers.UpdateBeerDtoAdmin;
import com.beer_tag.models.dto.users.CreateUserDto;
import com.beer_tag.models.dto.users.DeleteUserDto;
import com.beer_tag.models.dto.users.UpdateUserFromAdminDto;
import com.beer_tag.models.dto.users.UserFilterDto;
import com.beer_tag.models.impl.Beer;
import com.beer_tag.models.impl.User;
import com.beer_tag.services.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static com.beer_tag.heplers.GlobalConstants.*;
import static org.springframework.http.HttpHeaders.REFERER;

@Controller
@SessionAttributes({USER_FILTER_SESSION, BEER_FILTER_SESSION})
@RequestMapping("/admin")
public class AdminPortalController {

    private final BeerServiceFinder beerServiceFinder;
    private final BeerServiceActions beerServiceActions;
    private final UserServiceFinder userServiceFinder;
    private final UserServiceActions userServiceActions;
    private final CountryService countryService;
    private final StyleService styleService;
    private final TagService tagService;
    private final AdminServiceImpl adminService;
    private final MVCHelper mvcHelper;

    @Autowired
    public AdminPortalController(BeerServiceFinder beerServiceFinder, BeerServiceActions beerServiceActions,
                                 UserServiceActions userServiceActions, UserServiceFinder userServiceFinder,
                                 CountryService countryService, StyleService styleService, TagService tagService, MVCHelper mvcHelper,
                                 AdminServiceImpl adminService) {
        this.beerServiceFinder = beerServiceFinder;
        this.beerServiceActions = beerServiceActions;
        this.userServiceActions = userServiceActions;
        this.userServiceFinder = userServiceFinder;
        this.countryService = countryService;
        this.styleService = styleService;
        this.tagService = tagService;
        this.mvcHelper = mvcHelper;
        this.adminService = adminService;
    }

    @GetMapping
    public String showAdminPortal(@SessionAttribute(value = USER_FILTER_SESSION, required = false) UserFilterDto userFilter,
                                  @SessionAttribute(value = BEER_FILTER_SESSION, required = false) BeerFilterDto beerFilter,
                                  @RequestParam Optional<Integer> pageUsers, @RequestParam Optional<Integer> sizeUsers,
                                  @RequestParam Optional<Integer> pageBeers, @RequestParam Optional<Integer> sizeBeers,
                                  Model model, Principal principal, RedirectAttributes redirected) {

        int currentPageUsers = pageUsers.orElse(START_PAGE_PAGINATION);
        int pageSizeUsers = sizeUsers.orElse(NUMBER_OF_ITEMS_PER_PAGE_PAGINATION);
        int currentPageBeers = pageBeers.orElse(START_PAGE_PAGINATION);
        int pageSizeBeers = sizeBeers.orElse(NUMBER_OF_ITEMS_PER_PAGE_PAGINATION);

        User loggedUser;
        try {
            loggedUser = userServiceFinder.getByUsername(principal.getName());
        } catch (EntityNotFoundException e) {
            return mvcHelper.redirectError(redirected, e, "/admin");
        }
        List<Beer> allBeers = Objects.isNull(beerFilter) ? beerServiceFinder.getAll() : beerServiceFinder.filter(beerFilter);
        List<User> allUsers = Objects.isNull(userFilter) ? userServiceFinder.getAll() : userServiceFinder.filter(userFilter);

        Page<User> userPage = userServiceFinder.findPaginated(PageRequest.of(currentPageUsers - 1, pageSizeUsers), allUsers);
        Page<Beer> beerPage = beerServiceFinder.findPaginated(PageRequest.of(currentPageBeers - 1, pageSizeBeers), allBeers);

        int totalPagesUsers = userPage.getTotalPages();
        int totalPagesBeers = beerPage.getTotalPages();

        addAttributesForAdminPage(model, currentPageUsers, currentPageBeers, loggedUser,
                allBeers, allUsers, userPage, beerPage, totalPagesUsers, totalPagesBeers);

        return "admin";
    }

    @GetMapping("/beers-filtered")
    public String getFilteredBeers(BeerFilterDto beerFilter, Model model) {

        List<Beer> allBeers = Objects.isNull(beerFilter) ? beerServiceFinder.getAll() : beerServiceFinder.filter(beerFilter);
        Page<Beer> beerPage = beerServiceFinder.findPaginated(PageRequest.of(0, NUMBER_OF_ITEMS_PER_PAGE_PAGINATION), allBeers);

        int totalPagesBeers = beerPage.getTotalPages();

        addPagesToModelAttribute(model, totalPagesBeers, PAGE_NUMBER_BEERS);

        model.addAttribute(BEER_FILTER_SESSION, beerFilter);
        model.addAttribute(CURRENT_PAGE_BEERS, START_PAGE_PAGINATION);
        model.addAttribute(BEER_PAGE, beerPage);

        return "admin::beer-results";
    }

    @GetMapping("/users-filtered")
    public String getFilteredUsers(UserFilterDto userFilter, Model model) {

        List<User> allUsers = Objects.isNull(userFilter) ? userServiceFinder.getAll() : userServiceFinder.filter(userFilter);
        Page<User> userPage = userServiceFinder.findPaginated(PageRequest.of(0, NUMBER_OF_ITEMS_PER_PAGE_PAGINATION), allUsers);

        int totalPagesUsers = userPage.getTotalPages();

        addPagesToModelAttribute(model, totalPagesUsers, PAGE_NUMBER_USERS);

        model.addAttribute(USER_FILTER_SESSION, userFilter);
        model.addAttribute(CURRENT_PAGE_USERS, START_PAGE_PAGINATION);
        model.addAttribute(USER_PAGE, userPage);

        return "admin::user-results";
    }

    @GetMapping("/beers-pagination")
    public String changeBeersPaginationPage(@SessionAttribute(value = "beerFilterSession", required = false) BeerFilterDto beerFilter,
                                            @RequestParam Optional<Integer> pageBeers, @RequestParam Optional<Integer> sizeBeers, Model model) {

        int currentPageBeers = pageBeers.orElse(START_PAGE_PAGINATION);
        int pageSizeBeers = sizeBeers.orElse(NUMBER_OF_ITEMS_PER_PAGE_PAGINATION);

        List<Beer> allBeers = Objects.isNull(beerFilter) ? beerServiceFinder.getAll() : beerServiceFinder.filter(beerFilter);
        Page<Beer> beerPage = beerServiceFinder.findPaginated(PageRequest.of(currentPageBeers - 1, pageSizeBeers), allBeers);

        int totalPagesBeers = beerPage.getTotalPages();

        addPagesToModelAttribute(model, totalPagesBeers, PAGE_NUMBER_BEERS);

        model.addAttribute(CURRENT_PAGE_BEERS, currentPageBeers);
        model.addAttribute(BEER_PAGE, beerPage);

        return "admin::beer-results";
    }

    @GetMapping("/users-pagination")
    public String changeUsersPaginationPage(@SessionAttribute(value = "userFilterSession", required = false) UserFilterDto userFilter,
                                            @RequestParam Optional<Integer> pageUsers, @RequestParam Optional<Integer> sizeUsers, Model model) {

        int currentPageUsers = pageUsers.orElse(START_PAGE_PAGINATION);
        int pageSizeUsers = sizeUsers.orElse(NUMBER_OF_ITEMS_PER_PAGE_PAGINATION);
        List<User> allUsers = Objects.isNull(userFilter) ? userServiceFinder.getAll() : userServiceFinder.filter(userFilter);
        Page<User> userPage = userServiceFinder.findPaginated(PageRequest.of(currentPageUsers - 1, pageSizeUsers), allUsers);

        int totalPagesUsers = userPage.getTotalPages();

        addPagesToModelAttribute(model, totalPagesUsers, PAGE_NUMBER_USERS);

        model.addAttribute(CURRENT_PAGE_USERS, currentPageUsers);
        model.addAttribute(USER_PAGE, userPage);

        return "admin::user-results";
    }

    @PostMapping("/profile/create")
    public String handleUserCreate(@ModelAttribute CreateUserDto creationData, BindingResult bindingResult,
                                   RedirectAttributes redirected, Principal principal, HttpServletRequest request) {
        String redirectUrl = request.getHeader(REFERER);
        if (bindingResult.hasErrors())
            return mvcHelper.redirectError(redirected, bindingResult, redirectUrl);
        try {
            userServiceActions.createUser(creationData);
        } catch (EntityNotFoundException e) {
            return mvcHelper.redirectError(redirected, e, redirectUrl);
        }
        return mvcHelper.redirectSuccess(redirected, USER_ENTITY + SUCCESSFULLY_CREATED, redirectUrl);
    }

    @PostMapping("/userEdit")
    public String handleUserEdit(@ModelAttribute UpdateUserFromAdminDto updateData, RedirectAttributes redirected, Principal principal,
                                 BindingResult bindingResult, HttpServletRequest request) {
        String redirectUrl = request.getHeader(REFERER);
        if (bindingResult.hasErrors()) {
            return mvcHelper.redirectError(redirected, bindingResult, redirectUrl);
        }
        try {
            User loggedUser = userServiceFinder.getByUsername(principal.getName());
            adminService.updateUser(updateData, loggedUser);
        } catch (EntityNotFoundException e) {
            return mvcHelper.redirectError(redirected, e, redirectUrl);
        }
        return mvcHelper
                .redirectSuccess(redirected,
                        String.format(USER_ENTITY + WITH_NAME + SUCCESSFULLY_UPDATED, updateData.getUsername()), redirectUrl);
    }

    @PostMapping("/beerEdit")
    public String handleBeerEdit(@Validated @ModelAttribute CreateBeerDto beerDto, RedirectAttributes redirected, Principal
            principal, BindingResult bindingResult, HttpServletRequest request) {
        String redirectUrl = request.getHeader(REFERER);
        if (bindingResult.hasErrors()) {
            return mvcHelper.redirectError(redirected, bindingResult, redirectUrl);
        }
        try {
            User loggedUser = userServiceFinder.getByUsername(principal.getName());
            beerServiceActions.update(beerDto, loggedUser);

        } catch (EntityNotFoundException e) {
            return mvcHelper.redirectError(redirected, e, redirectUrl);
        }
        return mvcHelper.redirectSuccess(redirected,
                String.format(BEER_ENTITY + WITH_ID + SUCCESSFULLY_UPDATED, beerDto.getId()), redirectUrl);
    }

    @GetMapping("/{id}/deactivateUser")
    public String handleUserDeactivation(@PathVariable int id, RedirectAttributes redirected, Principal principal,
                                         HttpServletRequest request) {
        String redirectUrl = request.getHeader(REFERER);

        try {
            DeleteUserDto userDto = new DeleteUserDto(id);
            User loggedUser = userServiceFinder.getByUsername(principal.getName());
            userServiceActions.deactivate(userDto, loggedUser);
        } catch (EntityNotFoundException e) {
            return mvcHelper.redirectError(redirected, e, redirectUrl);
        }
        return mvcHelper.redirectSuccess(redirected, String.format(USER_ENTITY + WITH_ID + SUCCESSFULLY_DELETED, id), redirectUrl);
    }

    @GetMapping("/{id}/activateUser")
    public String handleUserActivation(@PathVariable int id, RedirectAttributes redirected, Principal principal,
                                       HttpServletRequest request) {
        String redirectUrl = request.getHeader(REFERER);
        try {
            User loggedUser = userServiceFinder.getByUsername(principal.getName());
            userServiceActions.activate(id, loggedUser);
        } catch (EntityNotFoundException e) {
            return mvcHelper.redirectError(redirected, e, redirectUrl);
        }
        return mvcHelper.redirectSuccess(redirected, String.format(USER_ENTITY + WITH_ID + SUCCESSFULLY_ACTIVATE, id), redirectUrl);
    }

    @GetMapping("/{id}/activateBeer")
    public String activateBeer(@PathVariable int id, RedirectAttributes redirected, Principal principal, HttpServletRequest request) {
        String redirectUrl = request.getHeader(REFERER);
        try {
            User loggedUser = userServiceFinder.getByUsername(principal.getName());
            beerServiceActions.activate(id, loggedUser);
        } catch (EntityNotFoundException e) {
            return mvcHelper.redirectError(redirected, e, redirectUrl);
        }
        return mvcHelper.redirectSuccess(redirected, String.format(BEER_ENTITY + WITH_ID + SUCCESSFULLY_ACTIVATE, id), redirectUrl);
    }

    @GetMapping("/{id}/deactivateBeer")
    public String deactivateBeer(@PathVariable int id, RedirectAttributes redirected, Principal principal, HttpServletRequest request) {
        String redirectUrl = request.getHeader(REFERER);
        try {
            User user = userServiceFinder.getByUsername(principal.getName());
            beerServiceActions.deactivate(id, user);
        } catch (EntityNotFoundException e) {
            return mvcHelper.redirectError(redirected, e, redirectUrl);
        }
        return mvcHelper.redirectSuccess(redirected, String.format(BEER_ENTITY + WITH_ID + SUCCESSFULLY_DELETED, id), redirectUrl);
    }

    private void addAttributesForAdminPage(Model model, int currentPageUsers, int currentPageBeers, User loggedUser,
                                           List<Beer> allBeers, List<User> allUsers, Page<User> userPage, Page<Beer> beerPage, int totalPagesUsers,
                                           int totalPagesBeers) {
        addPagesToModelAttribute(model, totalPagesUsers, PAGE_NUMBER_USERS);
        addPagesToModelAttribute(model, totalPagesBeers, PAGE_NUMBER_BEERS);

        model.addAttribute(CURRENT_PAGE_USERS, currentPageUsers);
        model.addAttribute(CURRENT_PAGE_BEERS, currentPageBeers);

        model.addAttribute(LOGGED_USER, loggedUser);
        model.addAttribute(USER_PAGE, userPage);
        model.addAttribute(BEER_PAGE, beerPage);
        model.addAttribute(ALL_BEERS, allBeers);
        model.addAttribute(ALL_USERS, allUsers);
        model.addAttribute(TAGS, tagService.getAll());
        model.addAttribute(STYLES, styleService.getAll());
        model.addAttribute(COUNTRIES, countryService.getAll());
        model.addAttribute(USER_FILTER_DTO, new UserFilterDto());
        model.addAttribute(BEER_FILTER_DTO, new BeerFilterDto());
        model.addAttribute(UPDATE_USER_DTO, new UpdateUserFromAdminDto());
        model.addAttribute(UPDATE_BEER_DTO, new UpdateBeerDtoAdmin());
        model.addAttribute(CREATE_USER_DTO, new CreateUserDto());
        model.addAttribute(CREATE_BEER_DTO, new CreateBeerDto());
    }

    private void addPagesToModelAttribute(Model model, int totalPagesUsers, String modelName) {
        if (totalPagesUsers > 0) {
            List<Integer> pageNumbers = IntStream.rangeClosed(1, totalPagesUsers).boxed().collect(Collectors.toList());
            model.addAttribute(modelName, pageNumbers);
        }
    }
}
