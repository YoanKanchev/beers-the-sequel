package com.beer_tag.controllers.mvc;

import com.beer_tag.models.dto.beers.BeerFilterDto;
import com.beer_tag.models.impl.Beer;
import com.beer_tag.services.BeerServiceFinder;
import com.beer_tag.services.CountryService;
import com.beer_tag.services.StyleService;
import com.beer_tag.services.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;
import java.util.Objects;

import static com.beer_tag.heplers.GlobalConstants.*;

@ControllerAdvice
@SessionAttributes("beerFilterSession")
@RequestMapping("/")
public class HomeController {

    private final BeerServiceFinder beerServiceFinder;

    private final CountryService countryService;
    private final StyleService styleService;
    private final TagService tagService;

    @Autowired
    public HomeController(BeerServiceFinder beerServiceFinder, CountryService countryService, StyleService styleService,
                          TagService tagService) {
        this.beerServiceFinder = beerServiceFinder;
        this.countryService = countryService;
        this.styleService = styleService;
        this.tagService = tagService;
    }

    @GetMapping
    public String showHomePage(@SessionAttribute(value = "beerFilterSession", required = false) BeerFilterDto beerDto, Model model) {
        List<Beer> beers = Objects.isNull(beerDto) ? beerServiceFinder.filter(new BeerFilterDto(true)) : beerServiceFinder.filter(beerDto);

        addCommonModelAttributes(model, beers);
        model.addAllAttributes(Map.of(
                TAGS, tagService.getAll(),
                STYLES, styleService.getAll(),
                COUNTRIES, countryService.getAll()));
        return "index";
    }

    @PostMapping
    public String showSearchResult(@ModelAttribute BeerFilterDto beerDto, Model model) {
        model.addAttribute("beerFilterSession", beerDto);
        return "redirect:/";
    }

    @GetMapping("filter")
    public String showSearchResultDynamic(BeerFilterDto beerDto, Model model) {
        beerDto.setIsActive(true);
        List<Beer> beers = beerServiceFinder.filter(beerDto);

        addCommonModelAttributes(model, beers);
        model.addAttribute(BEER_FILTER_SESSION, beerDto);

        return "index::search-index";
    }

    private void addCommonModelAttributes(Model model, List<Beer> beers) {
        model.addAllAttributes(Map.of(BEERS, beers, SEARCH_BEER_DTO, new BeerFilterDto()));
    }

}
