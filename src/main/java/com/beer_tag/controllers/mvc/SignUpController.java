package com.beer_tag.controllers.mvc;

import com.beer_tag.exceptions.AuthorisationException;
import com.beer_tag.exceptions.DuplicateEntityException;
import com.beer_tag.exceptions.EntityNotFoundException;
import com.beer_tag.heplers.MVCHelper;
import com.beer_tag.models.dto.EmailDto;
import com.beer_tag.models.dto.users.CreateUserDto;
import com.beer_tag.models.dto.users.UpdateForgottenPasswordDto;
import com.beer_tag.models.impl.User;
import com.beer_tag.models.impl.UserToken;
import com.beer_tag.services.TokenService;
import com.beer_tag.services.UserServiceActions;
import com.beer_tag.services.UserServiceFinder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.mail.MessagingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;

import static com.beer_tag.heplers.GlobalConstants.*;
import static org.springframework.http.HttpHeaders.REFERER;

@Controller
@RequestMapping("/sign-up")
public class SignUpController {

    private final UserServiceFinder userServiceFinder;
    private final UserServiceActions userServiceActions;
    private final TokenService tokenService;
    private final MVCHelper mvcHelper;

    @Autowired
    public SignUpController(UserServiceFinder userServiceFinder, UserServiceActions userServiceActions,
                            TokenService tokenService, MVCHelper mvcHelper) {

        this.userServiceFinder = userServiceFinder;
        this.userServiceActions = userServiceActions;
        this.tokenService = tokenService;
        this.mvcHelper = mvcHelper;
    }

    @GetMapping
    public String showRegisterPage(Model model) {
        model.addAttribute(USER, new CreateUserDto());
        return "sign-up-page";
    }

    @PostMapping
    public String registerUser(@Valid @ModelAttribute CreateUserDto userDto, BindingResult bindingResult,
                               RedirectAttributes redirectAttributes, HttpServletRequest servletRequest) {
        User user;
        if (bindingResult.hasErrors())
            return mvcHelper.redirectError(redirectAttributes, bindingResult, "/sign-up");
        try {
            user = userServiceActions.createUser(userDto);
        } catch (EntityNotFoundException | DuplicateEntityException e) {
            return mvcHelper.redirectError(redirectAttributes, e, "/sign-up");
        }
        return sendRegistrationConfirmationEmail(user.getEmail(), redirectAttributes, servletRequest);
    }

    @PostMapping("/send-confirmation")
    public String sendRegistrationConfirmationEmail(@RequestParam String email, RedirectAttributes redirectAttributes,
                                                    HttpServletRequest servletRequest) {
        String urlForMail = buildUrl(servletRequest);
        String redirectUrl = servletRequest.getHeader(REFERER);
        try {
            User user = userServiceFinder.getByEmail(email);
            UserToken token = tokenService.newToken(user);
            userServiceActions.sendRegistrationConfirmationEmail(token, urlForMail);
        } catch (EntityNotFoundException | MessagingException e) {
            return mvcHelper.redirectError(redirectAttributes, e, redirectUrl);
        }
        return mvcHelper.redirectSuccess(redirectAttributes, CONFIRMATION_EMAIL_SEND, "/");
    }

    @GetMapping("/verify")
    public String verifyUser(@RequestParam String token, RedirectAttributes redirectAttributes) {
        User user;
        try {
            UserToken userToken = tokenService.searchByName(token);
            user = userToken.getUser();

            if (userToken.isExpired()) {
                redirectAttributes.addFlashAttribute("buttonProperties",
                        new HashMap<>(Map.of("URL", "/sign-up/send-confirmation", "value", user.getEmail())));
                return mvcHelper.redirectError(redirectAttributes, YOUR_LINK_HAS_EXPIRED, "/");
            }
            userServiceActions.activateWithToken(user, userToken);
        } catch (EntityNotFoundException e) {
            return mvcHelper.redirectError(redirectAttributes, e, "/");
        }
        return mvcHelper
                .redirectSuccess(redirectAttributes, "You have successfully activated your profile. Welcome to Beers!!", "/");
    }

    @GetMapping("/recover-password")
    public String recoverPasswordShow(Model model) {
        model.addAttribute("emailDto", new EmailDto());
        return "recover-password-page";
    }

    @PostMapping("/recover-password")
    public String recoverPasswordHandle(@RequestParam String email, RedirectAttributes redirectAttributes,
                                        HttpServletRequest servletRequest) {
        String urlForMail = buildUrl(servletRequest);
        String redirectUrl = servletRequest.getHeader(REFERER);
        try {
            User user = userServiceFinder.getByEmail(email);
            UserToken token = tokenService.newToken(user);
            userServiceActions.sendPasswordRecoveryMail(token, urlForMail);
        } catch (EntityNotFoundException | MessagingException e) {
            return mvcHelper.redirectError(redirectAttributes, e, redirectUrl);
        }
        return mvcHelper.redirectSuccess(redirectAttributes, EMAIL_SEND, redirectUrl);
    }

    @GetMapping("/recover-password/verify")
    public String recoverPasswordValidate(@RequestParam String token, RedirectAttributes redirectAttributes, Model model) {
        User user;
        try {
            UserToken userToken = tokenService.searchByName(token);
            user = userToken.getUser();

            if (userToken.isExpired()) {
                redirectAttributes.addFlashAttribute("buttonProperties",
                        Map.of("URL", "/sign-up/recover-password", "value", user.getEmail()));
                return mvcHelper.redirectError(redirectAttributes, YOUR_LINK_HAS_EXPIRED, "/");
            }

        } catch (EntityNotFoundException e) {
            return mvcHelper.redirectError(redirectAttributes, e, "/");
        }
        model.addAttribute(USER, user);
        model.addAttribute("updatePasswordDto", new UpdateForgottenPasswordDto(user.getId(), token));

        return "reset-password-page";
    }

    @PostMapping("/recover-password/change")
    public String changeRecoveredPassword(@Valid @ModelAttribute UpdateForgottenPasswordDto passwordDto, BindingResult bindingResult,
                                          RedirectAttributes redirectAttributes, HttpServletRequest servletRequest) {
        String redirectUrl = servletRequest.getHeader(REFERER);
        if (bindingResult.hasErrors())
            return mvcHelper.redirectError(redirectAttributes, bindingResult, redirectUrl);
        User user;
        try {
            userServiceActions.updateForgottenPassword(passwordDto);
            user = userServiceFinder.getById(passwordDto.getId());
        } catch (EntityNotFoundException | AuthorisationException e) {
            return mvcHelper.redirectError(redirectAttributes, e, "/");
        }
        logIntoSpring(servletRequest, user.getUsername(), passwordDto.getNewPassword());

        return mvcHelper.redirectSuccess(redirectAttributes, "You have successfully recovered your password!", "/");
    }

    private void logIntoSpring(HttpServletRequest request, String username, String password) {
        try {
            request.login(username, password);
        } catch (ServletException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e);
        }
    }

    private String buildUrl(HttpServletRequest request) {
        return "http://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath();
    }
}
