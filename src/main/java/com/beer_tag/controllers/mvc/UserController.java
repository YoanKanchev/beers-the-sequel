package com.beer_tag.controllers.mvc;

import com.beer_tag.exceptions.AuthorisationException;
import com.beer_tag.exceptions.EntityNotFoundException;
import com.beer_tag.heplers.MVCHelper;
import com.beer_tag.models.dto.UpdatePasswordDto;
import com.beer_tag.models.dto.users.DeleteUserDto;
import com.beer_tag.models.dto.users.UpdateUserDto;
import com.beer_tag.models.impl.User;
import com.beer_tag.services.UserServiceActions;
import com.beer_tag.services.UserServiceFinder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;
import java.util.Objects;

@Controller
@RequestMapping(value = "/users")
public class UserController {

    private final UserServiceFinder userServiceFinder;
    private final UserServiceActions userServiceActions;
    private final MVCHelper mvcHelper;

    @Autowired
    public UserController(UserServiceFinder userServiceFinder, UserServiceActions userServiceActions, MVCHelper mvcHelper) {
        this.userServiceFinder = userServiceFinder;
        this.userServiceActions = userServiceActions;
        this.mvcHelper = mvcHelper;
    }

    @GetMapping("/personal/profile")
    public String showPersonalProfile(Model model, Principal principal) {

        User user = userServiceFinder.getByUsername(principal.getName());

        model.addAttribute("isPersonal", true);
        model.addAttribute("user", user);
        model.addAttribute("updateUserDto", new UpdateUserDto());
        model.addAttribute("updatePasswordDto", new UpdatePasswordDto());
        model.addAttribute("deleteUserDto", new DeleteUserDto());

        return "user-profile-page";
    }

    @GetMapping("/{id}")
    public String showOthersProfile(@PathVariable int id, Model model, Principal principal) {
        try {
            if (!Objects.isNull(principal)) {
                User loggedUser = userServiceFinder.getByUsername(principal.getName());
                if (id == loggedUser.getId())
                    return "redirect:/users/profile";
            }
            User user = userServiceFinder.getById(id);
            model.addAttribute("user", user);
            model.addAttribute("isPersonal", false);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage(), e);
        }
        return "user-profile-page";
    }


    @PostMapping("/profile/update")
    public String handleProfileUpdate(@ModelAttribute UpdateUserDto updateData, BindingResult bindingResult,
                                      RedirectAttributes redirected, Principal principal, HttpServletRequest request) {
        String redirectUrl = request.getHeader("referer");
        if (bindingResult.hasErrors())
            return mvcHelper.redirectError(redirected, bindingResult, redirectUrl);

        try {
            User loggedUser = userServiceFinder.getByUsername(principal.getName());
            userServiceActions.update(updateData, loggedUser);
        } catch (EntityNotFoundException e) {
            return mvcHelper.redirectError(redirected, e, redirectUrl);
        }
        return mvcHelper.redirectSuccess(redirected,
                "You have updated your profile", redirectUrl);
    }

    @PostMapping("/profile/update/password")
    public String handlePasswordUpdate(@ModelAttribute UpdatePasswordDto updateData, BindingResult bindingResult,
                                       RedirectAttributes redirected, Principal principal, HttpServletRequest request) {
        String redirectUrl = request.getHeader("referer");
        if (bindingResult.hasErrors())
            return mvcHelper.redirectError(redirected, bindingResult, redirectUrl);

        try {
            User loggedUser = userServiceFinder.getByUsername(principal.getName());
            userServiceActions.passwordUpdate(updateData, loggedUser);
        } catch (EntityNotFoundException e) {
            return mvcHelper.redirectError(redirected, e, redirectUrl);
        }
        return mvcHelper.redirectSuccess(redirected,
                "You have updated your profile", redirectUrl);
    }

    @PostMapping("/profile/delete")
    public String deleteProfile(@ModelAttribute DeleteUserDto dto, RedirectAttributes redirectAttributes,
                                Principal principal, HttpServletRequest servletRequest) {
        String redirectUrl = servletRequest.getHeader("referer");
        try {
            User loggedUser = userServiceFinder.getByUsername(principal.getName());
            userServiceActions.deactivate(dto, loggedUser);
        } catch (EntityNotFoundException | AuthorisationException e) {
            return mvcHelper.redirectError(redirectAttributes, e, redirectUrl);
        }
        return "redirect:/logout";
    }


}
