package com.beer_tag.controllers.mvc;

import com.beer_tag.exceptions.AuthorisationException;
import com.beer_tag.exceptions.DuplicateEntityException;
import com.beer_tag.exceptions.EntityNotFoundException;
import com.beer_tag.heplers.MVCHelper;
import com.beer_tag.models.dto.beers.BeerFilterDto;
import com.beer_tag.models.dto.beers.CreateBeerDto;
import com.beer_tag.models.dto.beers.CreateRatingDto;
import com.beer_tag.models.impl.Beer;
import com.beer_tag.models.impl.Rating;
import com.beer_tag.models.impl.Tag;
import com.beer_tag.models.impl.User;
import com.beer_tag.services.*;
import com.beer_tag.services.mapping.ModelMapperService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.security.Principal;
import java.util.Map;

import static com.beer_tag.heplers.GlobalConstants.*;
import static org.springframework.http.HttpHeaders.REFERER;

@Controller
@RequestMapping("/beers")
public class BeerController {

    private final BeerServiceFinder beerServiceFinder;
    private final UserServiceFinder userServiceFinder;
    private final BeerServiceActions beerServiceActions;
    private final UserServiceActions userServiceActions;
    private final StyleService styleService;
    private final CountryService countryService;
    private final ModelMapperService modelMapperService;
    private final MVCHelper mvcHelper;

    @Autowired
    public BeerController(BeerServiceFinder beerServiceFinder, UserServiceFinder userServiceFinder,
                          BeerServiceActions beerServiceActions, UserServiceActions userServiceActions, StyleService styleService,
                          CountryService countryService, ModelMapperService modelMapperService, MVCHelper mvcHelper) {
        this.beerServiceFinder = beerServiceFinder;
        this.userServiceFinder = userServiceFinder;
        this.beerServiceActions = beerServiceActions;
        this.userServiceActions = userServiceActions;
        this.styleService = styleService;
        this.countryService = countryService;
        this.modelMapperService = modelMapperService;
        this.mvcHelper = mvcHelper;
    }

    @GetMapping("/{id}")
    public String getBeerById(@PathVariable int id, Model model, Principal principal) {
        Beer beer;
        User loggedUser = null;

        try {
            BeerFilterDto beerFilterDto = new BeerFilterDto(id, true);
            beer = beerServiceFinder.filterOne(beerFilterDto);
            if (principal != null) {
                loggedUser = userServiceFinder.getByUsername(principal.getName());
            }
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
        addCommonModelAttributes(model);
        model.addAllAttributes(Map.of(
                BEER, beer,
                TAGS, beer.getTags(),
                SIMILAR_BEERS, beerServiceFinder.getSimilarBeers(beer),
                RATING, new Rating(),
                CREATE_TAG, new Tag()));
        model.addAttribute(LOGGED_USER, loggedUser);

        return "beer-page";
    }

    @GetMapping("/create")
    public String createBeer(Model model) {
        addCommonModelAttributes(model);
        return "create-beer-page";
    }

    @PostMapping("/create")
    public String handleCreateBeer(@Valid @ModelAttribute CreateBeerDto beerDto,
                                   BindingResult bindingResult, Principal principal, RedirectAttributes redirectAttributes, HttpServletRequest request) {
        String redirectUrl = request.getHeader(REFERER);
        if (bindingResult.hasErrors())
            return mvcHelper.redirectError(redirectAttributes, bindingResult, redirectUrl);

        try {
            User user = userServiceFinder.getByUsername(principal.getName());
            beerServiceActions.create(beerDto, user);
        } catch (DuplicateEntityException | EntityNotFoundException | AuthorisationException e) {
            return mvcHelper.redirectError(redirectAttributes, e, redirectUrl);
        }
        return mvcHelper.redirectSuccess(redirectAttributes,
                String.format(BEER_ENTITY + WITH_NAME + SUCCESSFULLY_CREATED, beerDto.getName()), "/");
    }

    @PostMapping("/{id}/delete")
    public String deleteBeer(@PathVariable int id, Principal principal, RedirectAttributes redirectAttributes,
                             HttpServletRequest request) {
        String redirectUrl = request.getHeader(REFERER);
        try {
            User loggedUser = userServiceFinder.getByUsername(principal.getName());
            beerServiceActions.deactivate(id, loggedUser);

        } catch (DuplicateEntityException | EntityNotFoundException | AuthorisationException e) {
            return mvcHelper.redirectError(redirectAttributes, e, redirectUrl);
        }
        return mvcHelper
                .redirectSuccess(redirectAttributes, String.format(BEER_ENTITY + WITH_ID + SUCCESSFULLY_DELETED, id), redirectUrl);
    }

    @PostMapping("/{id}/update")
    public String updateBeer(@Valid @PathVariable int id, @ModelAttribute CreateBeerDto beerDto, RedirectAttributes redirectAttributes,
                             BindingResult bindingResult, Principal principal, HttpServletRequest request) {
        String redirectUrl = request.getHeader(REFERER);

        if (bindingResult.hasErrors())
            return mvcHelper.redirectError(redirectAttributes, bindingResult, redirectUrl);

        try {
            User loggedUser = userServiceFinder.getByUsername(principal.getName());
            beerServiceActions.update(beerDto, loggedUser);

        } catch (DuplicateEntityException | EntityNotFoundException | AuthorisationException e) {
            return mvcHelper.redirectError(redirectAttributes, e, redirectUrl);
        }
        return mvcHelper
                .redirectSuccess(redirectAttributes, String.format(BEER_ENTITY + WITH_ID + SUCCESSFULLY_UPDATED, id), redirectUrl);
    }

    @PostMapping("/{id}/rate")
    public String rateBeer(@PathVariable int id, @Valid @ModelAttribute CreateRatingDto ratingDto, BindingResult bindingResult,
                           Principal principal, RedirectAttributes redirectAttributes, HttpServletRequest request) {
        String redirectUrl = request.getHeader(REFERER);
        if (bindingResult.hasErrors())
            return mvcHelper.redirectError(redirectAttributes, bindingResult, redirectUrl);

        try {
            BeerFilterDto beerFilterDto = new BeerFilterDto(id, true);
            Beer beer = beerServiceFinder.filterOne(beerFilterDto);
            User loggedUser = userServiceFinder.getByUsername(principal.getName());
            Rating rating = modelMapperService.toRating(ratingDto, beer, loggedUser);
            beerServiceActions.rateBeer(beer, rating, loggedUser);

        } catch (DuplicateEntityException | EntityNotFoundException | AuthorisationException e) {
            return mvcHelper.redirectError(redirectAttributes, e, redirectUrl);
        }
        return mvcHelper
                .redirectSuccess(redirectAttributes, String.format(BEER_ENTITY + WITH_ID + SUCCESSFULLY_RATED, id), redirectUrl);
    }

    @PostMapping("/{id}/delete-rating")
    public String deleteBeerRating(@PathVariable int id, @RequestParam Integer ratingId, Principal principal,
                                   RedirectAttributes redirectAttributes, HttpServletRequest request) {
        String redirectUrl = request.getHeader(REFERER);
        try {
            BeerFilterDto beerFilterDto = new BeerFilterDto(id, true);
            Beer beer = beerServiceFinder.filterOne(beerFilterDto);
            User loggedUser = userServiceFinder.getByUsername(principal.getName());
            beerServiceActions.removeRating(ratingId, beer, loggedUser);

        } catch (DuplicateEntityException | EntityNotFoundException | AuthorisationException e) {
            return mvcHelper.redirectError(redirectAttributes, e, redirectUrl);
        }
        return mvcHelper.redirectSuccess(redirectAttributes, RATING_ENTITY + SUCCESSFULLY_DELETED, redirectUrl);
    }

    @PostMapping("/{id}/tag")
    public String tagBeer(@Valid @PathVariable int id, @ModelAttribute Tag tag, RedirectAttributes redirectAttributes,
                          Principal principal, HttpServletRequest request) {
        String redirectUrl = request.getHeader(REFERER);
        try {
            BeerFilterDto beerFilterDto = new BeerFilterDto(id, true);
            Beer beerToTag = beerServiceFinder.filterOne(beerFilterDto);
            User loggedUser = userServiceFinder.getByUsername(principal.getName());
            beerServiceActions.tagBeer(beerToTag, tag, loggedUser);

        } catch (DuplicateEntityException | EntityNotFoundException | AuthorisationException e) {
            return mvcHelper.redirectError(redirectAttributes, e, redirectUrl);
        }
        return mvcHelper
                .redirectSuccess(redirectAttributes, String.format(BEER_ENTITY + WITH_ID + SUCCESSFULLY_CREATED, id), redirectUrl);
    }

    @PostMapping("/{id}/favourite")
    public String addRemoveFromFavourite(@PathVariable int id, Principal principal, HttpServletRequest request) {
        User user = userServiceFinder.getByUsername(principal.getName());
        BeerFilterDto beerFilterDto = new BeerFilterDto(id, true);
        Beer beer = beerServiceFinder.filterOne(beerFilterDto);
        userServiceActions.addRemoveBeerFromFavourite(beer, user);

        return REDIRECT + request.getHeader(REFERER);
    }

    @PostMapping("/{id}/drunk")
    public String addRemoveFromDrunk(@PathVariable int id, Principal principal, HttpServletRequest request) {
        User user = userServiceFinder.getByUsername(principal.getName());
        BeerFilterDto beerFilterDto = new BeerFilterDto(id, true);
        Beer beer = beerServiceFinder.filterOne(beerFilterDto);
        userServiceActions.addRemoveBeerToDrunk(beer, user);

        return REDIRECT + request.getHeader(REFERER);
    }

    private void addCommonModelAttributes(Model model) {
        model.addAttribute(COUNTRIES, countryService.getAll());
        model.addAttribute(STYLES, styleService.getAll());
        model.addAttribute(CREATE_BEER_DTO, new CreateBeerDto());
    }
}
