package com.beer_tag.controllers.error;

import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;

@ControllerAdvice
public class MVCErrorController implements ErrorController {

		@RequestMapping("/error")
		public String showErrorPage(HttpServletRequest request, Model model) {
				Object status = request.getAttribute(RequestDispatcher.ERROR_STATUS_CODE);

				if (status != null) {
						int statusCode = Integer.parseInt(status.toString());
						String message = request.getAttribute(RequestDispatcher.ERROR_MESSAGE).toString();
						model.addAttribute("code", statusCode);

						if (statusCode == HttpStatus.NOT_FOUND.value()) {
								model.addAttribute("message", message);
								return "errorPages/404errorPage";
						} else if (statusCode == HttpStatus.INTERNAL_SERVER_ERROR.value()) {
								model.addAttribute("message", message);
								return "errorPages/500errorPage";
						} else if (statusCode == HttpStatus.FORBIDDEN.value()) {
								model.addAttribute("message", message);
								return "errorPages/403errorPage";
						}
				}
				return "redirect:/";
		}

		@Override
		public String getErrorPath() {
				return null;
		}
}
