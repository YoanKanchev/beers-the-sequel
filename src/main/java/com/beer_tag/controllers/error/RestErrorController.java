package com.beer_tag.controllers.error;

import com.beer_tag.exceptions.AuthorisationException;
import com.beer_tag.exceptions.DuplicateEntityException;
import com.beer_tag.exceptions.EntityNotFoundException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice("com.beer_tag.controllers.rest")
class RestErrorController extends ResponseEntityExceptionHandler {

		@ExceptionHandler(value = EntityNotFoundException.class)
		protected ResponseEntity<Object> handleConflict(EntityNotFoundException ex, WebRequest request) {
				return handleExceptionInternal(ex, ex.getMessage(), new HttpHeaders(), HttpStatus.NOT_FOUND, request);
		}

		@ExceptionHandler(value = AuthorisationException.class)
		protected ResponseEntity<Object> handleConflict(AuthorisationException ex, WebRequest request) {
				return handleExceptionInternal(ex, ex.getMessage(), new HttpHeaders(), HttpStatus.UNAUTHORIZED, request);
		}

		@ExceptionHandler(value = DuplicateEntityException.class)
		protected ResponseEntity<Object> handleConflict(DuplicateEntityException ex, WebRequest request) {
				return handleExceptionInternal(ex, ex.getMessage(), new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
		}
}
