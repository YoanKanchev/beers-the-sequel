package com.beer_tag.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.thymeleaf.spring5.SpringTemplateEngine;
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;
import org.thymeleaf.templateresolver.ITemplateResolver;


public class ThymeleafConfig {

		public ResourceBundleMessageSource emailMessageSource() {
				ResourceBundleMessageSource messageSource = new ResourceBundleMessageSource();
				messageSource.setBasename("mailMessages");
				return messageSource;
		}

}
