const passwordInput = document.getElementById("password");
const passwordConfirmInput = document.getElementById("confirm_password");
const PASSWORDS_NOT_MATCH_TAG = document.createTextNode("<span class='danger-color'>Passwords don't match</span>");


passwordConfirmInput.addEventListener("blur", validatePassword)


function validatePassword() {
    debugger
    if (passwordInput.value !== passwordConfirmInput.value) {
        this.setCustomValidity("Passwords Don't Match");
        passwordConfirmInput.parentElement.parentElement.appendChild(PASSWORDS_NOT_MATCH_TAG)
    } else {
        this.setCustomValidity('');
    }
}


function validateUsername() {
    const username = jQuery("#username").val();
    const url = "/api/users/searchBy/";

    jQuery.ajax({
        url: url,
        method: "GET",
        type: "JSON",
        data: {username: username},
        success: function (data) {
            if (data) {
                const html = "<p id='usernameWarning' style='color: darkred'>" + username + " is already taken!</p>";
                $("#usernameWarning").remove();

                $(html).hide().appendTo("#usernameText").fadeIn(1000);
            } else {
                $("#usernameWarning").fadeOut("medium", function () {
                    $(this).remove();
                });
            }
        }
    });
}

function validateEmail() {
    debugger
    const email = $("#email").val();
    const url = "/api/users/existEmail";
    $.ajax({
        url: url,
        method: "GET",
        type: "JSON",
        data: {email: email},
        success: function (data) {
            if (!data) {
                debugger
                const html = "<p id='emailWarning' style='color: darkred'>User with email:" + email + " does not exist!</p>";
                $("#emailWarning").remove();

                $(html).hide().appendTo(("#emailParent")).fadeIn(1000);
            } else {
                $("#emailWarning").fadeOut("medium", function () {
                    $(this).remove();
                });
            }
        },
        error: function (data) {
            debugger
            var val = data
        }
    });
}